#ifndef COVIDMANAGER_SCELTA_GESTIONALE_H
#define COVIDMANAGER_SCELTA_GESTIONALE_H

#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
#include <QGroupBox>
#include <QPixmap>
#include <QIcon>
#include <iostream>
#include <model/gerarchia/OperatoreSanitario.h>
#include <QFile>

class Controller;

class Scelta_Gestionale: public QWidget {
    Q_OBJECT;
public:
    Scelta_Gestionale(Controller* controller= nullptr, QWidget *parent = nullptr);
    ~Scelta_Gestionale() = default;

    void nascondi_pulsante_vaccini();
    void nascondi_pulsante_test();
    void setOperatoreSanitario(OperatoreSanitario*);
    void setStyle();

public slots:
    void signGestionaleVax_and_send();
    void signGestionaleTest_and_send();

private:
    Controller *controller;
    QHBoxLayout *mainLayout;
    QVBoxLayout *vaccina, *testa;
    QPushButton *vax, *test;
    OperatoreSanitario *op;
    QGroupBox *gruppoBottoni;

signals:
    void signGestionaleVax(OperatoreSanitario*);
    void signGestionaleTamp(OperatoreSanitario*);
};


#endif //COVIDMANAGER_SCELTA_GESTIONALE_H
