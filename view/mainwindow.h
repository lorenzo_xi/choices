

#ifndef COVIDMANAGER_MAINWINDOW_H
#define COVIDMANAGER_MAINWINDOW_H

#include <QWidget>
#include <QVBoxLayout>
#include <QPushButton>
#include <QIcon>
#include <QLabel>
#include <model/gerarchia/Base.h>
#include <model/DeepPtr.h>
#include <model/Vector.h>


class Controller;

class MainWindow : public QWidget {
Q_OBJECT

public:
    MainWindow(Controller* controller= nullptr, QWidget *parent = nullptr);
    ~MainWindow() = default;
    void setStyle();


public slots:


private:
    Controller      *controller;

    QVBoxLayout     *mainLayout;

    QLabel          *prova;

    QPushButton     *entra;

    Vector<DeepPtr<Base>> listaOperatori;


signals:
    void signAccedi();

};


#endif //COVIDMANAGER_MAINWINDOW_H
