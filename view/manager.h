#ifndef COVIDMANAGER_MANAGER_H
#define COVIDMANAGER_MANAGER_H


#include <QWidget>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QMenuBar>
#include <QPushButton>
#include <QIcon>
#include <QFile>
#include <QDialog>
#include <QLabel>

#include <iostream>
#include <QLineEdit>
#include <QRadioButton>
#include <QGroupBox>
#include <QMessageBox>
#include <model/gerarchia/OperatoreSanitario.h>
#include <regex>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QDir>
#include <QDebug>
#include <QButtonGroup>
#include <model/DeepPtr.h>
#include <model/Vector.h>
#include <model/Algo/algorithm.h>


class Controller;

class Manager : public QWidget {
Q_OBJECT

public:
    Manager(Controller* controller= nullptr, QWidget *parent = nullptr);
    ~Manager() = default;
    QPushButton* getButton() const;
    void setStyle();

public slots:
    void check();
    void popup_login_failed();

signals:
    void signAccedi();
    void login_failed();
    OperatoreSanitario * login_operatore(OperatoreSanitario*);

private:
    Controller      *controller;

    QVBoxLayout     *mainLayout;
    QGridLayout     *gridLayout;

    QLabel          *title;
    QLineEdit       *inputfield;

    QPushButton     *enter;

    QMessageBox     *errorPopup_formatocf, *errorPopup_existence;

    OperatoreSanitario *op;

    void addTitle();

    void addInputField();

    void addPopups();

    void addConnections();

    bool getOpFromCf();

};

#endif //COVIDMANAGER_MANAGER_H
