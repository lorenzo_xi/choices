#include "manager.h"


Manager::Manager(Controller *c, QWidget *parent) : QWidget(parent), controller(c) {
    setFixedSize(500,390);
    setWindowTitle("Accedi");

    mainLayout = new QVBoxLayout(this);


    addTitle();

    addInputField();

    addPopups();

    addConnections();

    setStyle();

    setLayout(mainLayout);
}

void Manager::addTitle() {
    title = new QLabel();
    title->setPixmap(QPixmap("://resources/img/identificazione.png"));

    title->setAlignment(Qt::AlignCenter);
    mainLayout->addWidget(title);
}

void Manager::addInputField() {
    inputfield = new QLineEdit();
    inputfield->setText("");
    inputfield->setPlaceholderText("Inserisci il tuo Codice Fiscale");
    inputfield->setMaxLength(16);
    mainLayout->addWidget(inputfield);

    enter = new QPushButton("ACCEDI");
    mainLayout->addWidget(enter);

}

void Manager::addPopups() {
    errorPopup_formatocf = new QMessageBox();
    errorPopup_formatocf->setText("Errore");
    errorPopup_formatocf->setInformativeText("formato codice fiscale non valido");
    errorPopup_formatocf->setIcon(QMessageBox::Warning);

    errorPopup_existence = new QMessageBox();
    errorPopup_existence->setText("Errore");
    errorPopup_existence->setInformativeText("codice fiscale non presente nei nostri sistemi");
    errorPopup_existence->setIcon(QMessageBox::Critical);
}

bool Manager::getOpFromCf() {
    Vector<DeepPtr<Base>> v;
    Algorithm::readFile_toVectorDeepPtr(v,"://resources/jsondata/opsanitari.json","operatore_sanitario");
    bool trovato = false;

    for(auto it=v.begin(); it!=v.end() && !trovato; ++it){
        OperatoreSanitario* tmp = dynamic_cast<OperatoreSanitario*>(it->get())->clone();
        if(tmp->getCF() == inputfield->text().toStdString()){
            op = dynamic_cast<OperatoreSanitario*>(it->get())->clone();
            trovato = true;
        }
    }
    return trovato;
}

void Manager::addConnections() {
    connect(enter, SIGNAL(clicked()), this,SLOT(check()));
    connect(this, SIGNAL(login_failed()), this,SLOT(popup_login_failed()));

}
void Manager::setStyle(){
    QFile file("://resources/style/scelta.css");
    file.open(QFile::ReadOnly);
    QString styleSheet = QLatin1String(file.readAll());
    setStyleSheet(styleSheet);
}

void Manager::check() {
    if(!std::regex_match((inputfield->text()).toStdString(),
                         std::regex("[A-Z][A-Z][A-Z][A-Z][A-Z][A-Z][0-9][0-9][A-Z][0-9][0-9][A-Z][0-9][0-9][0-9][A-Z]"))) {
        errorPopup_formatocf->show();
    }else{
        if(getOpFromCf()){
            emit login_operatore(op);
        }else{
            emit login_failed();
        }
    }
}

void Manager::popup_login_failed() {
    errorPopup_existence->show();
    inputfield->setText("");
}

QPushButton *Manager::getButton() const{
    return enter;
}

