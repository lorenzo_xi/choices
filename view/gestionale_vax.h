
#ifndef CHOICES_GESTIONALE_VAX_H
#define CHOICES_GESTIONALE_VAX_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QLabel>
#include <QGroupBox>
#include <iostream>
#include <QBoxLayout>
#include "view/widget/V_form_segnalazione.h"
#include "view/widget/TV_listaVisite.h"

#include <QMessageBox>
#include <view/widget/TV_barraRicerca.h>
#include <view/widget/V_form_vaccini.h>
#include "widget/V_form_segnalazione.h"


class Controller;


class Gestionale_Vax: public QWidget{
    Q_OBJECT;
public:
    Gestionale_Vax(Controller* controller= nullptr, QWidget *parent = nullptr);
    ~Gestionale_Vax() = default;
    void setStyle();
    
    TV_barraRicerca *getBarraRicerca() const;
    TV_listaVisite *getlistavisite() const;
    unsigned int get_indice_visite() const;
    void set_indice_visite(unsigned int);
    void setOperatoreSanitario(OperatoreSanitario*);
    V_form_vaccini *getform() const;
    V_form_vaccini *getEmptyForm() const;
    OperatoreSanitario* get_logged_user() const;

public slots:
    void showSegnalazione();
    void showPazienteExtra();
    void showVaccina();
    void showVisite();
    void closeButton();
    void closeQuestion();


private:
    Controller      *controller;

    OperatoreSanitario *logged_user;

    QWidget         *widgetMenu, *widgetContenuto;

    QHBoxLayout     *mainLayout, *layoutLabel;

    QPushButton     *button1, *button2, *button3, *button4, *esci, *indietro;

    QGroupBox       *groupButton, *groupLabel;

    QVBoxLayout     *contenuto, *menu, *vbox;

    QLabel          *text, *nome, *cognome, *data_nascita, *CF, *richiamo, *tipoVaccino, *stato ;

    V_form_segnalazione *formSegnalazione;

    TV_barraRicerca*barraricerca;

    TV_listaVisite *listaVisite;

    QMessageBox *domandaChiusura;

    V_form_vaccini *formVax;

    V_form_vaccini *formVaxEmpty;

    unsigned int indice_visite;

    void setLayoutWidth();
    void addMenuButtons();
signals:
    void signIndietro();
};


#endif //CHOICES_GESTIONALE_VAX_H
