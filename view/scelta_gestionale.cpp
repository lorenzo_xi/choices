#include "scelta_gestionale.h"

Scelta_Gestionale::Scelta_Gestionale(Controller *c , QWidget *parent): QWidget(parent), controller(c) {
    setFixedSize(500,300);
    setWindowTitle("Scelta operazione");

    mainLayout = new QHBoxLayout(this);

    vaccina = new QVBoxLayout ();
    testa = new QVBoxLayout();

    test = new QPushButton ();
    test->setIcon(QIcon("://resources/img/bottone_testa.png"));
    test->setIconSize(QSize(200, 250));

    vax = new QPushButton ();
    vax->setIcon(QIcon("://resources/img/button_vax.png"));
    vax->setIconSize(QSize(200,250));

    testa->addWidget(test);
    vaccina->addWidget(vax);

    mainLayout->addLayout(vaccina);
    mainLayout->addLayout(testa);

    setLayout(mainLayout);
    setStyle();
    connect(vax, SIGNAL(clicked()), this, SLOT(signGestionaleVax_and_send()));
    connect(test, SIGNAL(clicked()), this, SLOT(signGestionaleTest_and_send()));
}

void Scelta_Gestionale::nascondi_pulsante_test() {
    vax->setEnabled(false);
}

void Scelta_Gestionale::nascondi_pulsante_vaccini() {
    vax->setVisible(false);
}

void Scelta_Gestionale::setOperatoreSanitario(OperatoreSanitario * tmp) {
    op = tmp->clone();
}

void Scelta_Gestionale::signGestionaleVax_and_send() {
    emit signGestionaleVax(op);
}

void Scelta_Gestionale::signGestionaleTest_and_send() {
    emit signGestionaleTamp(op);
}
void Scelta_Gestionale::setStyle(){
    QFile file("://resources/style/scelta.css");
    file.open(QFile::ReadOnly);
    QString styleSheet = QLatin1String(file.readAll());
    setStyleSheet(styleSheet);
}
