#include "gestionale_tamponi.h"
#include <QFile>

Gestionale_Tamponi::Gestionale_Tamponi(Controller *c , QWidget *parent): QWidget(parent), controller(c) {
    setFixedSize(900,500);
    setWindowTitle("Gestione Tamponi");

    mainLayout = new QHBoxLayout(this);

    menu = new QVBoxLayout ();
    contenuto = new QVBoxLayout();
    layoutLabel = new QHBoxLayout();
    groupLabel = new QGroupBox();

    CF = new QLabel ("Codice Fiscale");
    CF->setFixedWidth(200);
    CF->setObjectName("cf_t");
    data_nascita = new QLabel("Data Nascita");
    data_nascita->setObjectName("data_t");
    data_nascita->setFixedWidth(150);
    n = new QLabel ( "ID");
    n->setObjectName("n_t");
    n->setFixedWidth(20);
    email = new QLabel ("Email");
    email->setObjectName("email_t");
    email->setFixedWidth(200);
    recapito = new QLabel ("Telefono");
    recapito->setObjectName("recapito_t");
    recapito->setFixedWidth(150);
    tipo = new QLabel("Tipo Tampone");
    tipo->setObjectName("tipo_t");
    tipo->setFixedWidth(150);
    esito = new QLabel("Esito");
    esito->setObjectName("esito_t");
    esito->setFixedWidth(50);
    layoutLabel->addWidget(n);
    layoutLabel->addWidget(data_nascita);
    layoutLabel->addWidget(CF);
    layoutLabel->addWidget(email);
    layoutLabel->addWidget(recapito);
    layoutLabel->addWidget(tipo);
    layoutLabel->addWidget(esito);

    groupLabel->setLayout(layoutLabel);
    groupLabel->setFixedSize(1200,80);

    indietro = new QPushButton("← torna indietro");

    indice_visite = Algorithm::getIndex(false);
    listaVisite = new TV_listaVisite(c,false,false,"","visite_test.json","visite_test",-1);

    if(indice_visite < listaVisite->getListaSize() ){
        formTamponi = new T_form_test(c, this, true, indice_visite);
    }else{
        formTamponi = new T_form_test(c, this, false, indice_visite);
    }

    barraricerca  = new TV_barraRicerca(c, this,true);
    indietro = new QPushButton("← torna indietro");


    formTamponi->setVisible(false);
    barraricerca->setVisible(false);
    listaVisite->setVisible(false);
    groupLabel->setVisible(false);

    contenuto->addWidget(formTamponi);
    contenuto->addWidget(barraricerca);
    contenuto->addWidget(groupLabel);
    contenuto->addWidget(listaVisite);

    setLayoutWidth();
    addMenuButtons();

    mainLayout->addWidget(widgetMenu);
    mainLayout->addWidget(widgetContenuto);
    setLayout(mainLayout);


    connect(button1,SIGNAL(clicked()), this, SLOT(showTesta()));
    connect(button2, SIGNAL(clicked()), this, SLOT(showListaPazienti()));
    connect(esci, SIGNAL(clicked()), this, SLOT(closeQuestion()));
    connect(indietro, SIGNAL(clicked()), this, SIGNAL(signIndietro()));
}

void Gestionale_Tamponi::setLayoutWidth() {
    widgetMenu = new QWidget();
    widgetContenuto = new QWidget();
    menu->addWidget(indietro);
    widgetMenu->setFixedWidth(350);
    widgetContenuto->setLayout(contenuto);
    widgetContenuto->setMinimumWidth(400);
    widgetMenu->setLayout(menu);
}

void Gestionale_Tamponi::addMenuButtons() {
    text = new QLabel ();

    button1=  new QPushButton ("Testa");
    button2= new QPushButton("Lista Pazienti");
    button3 = new QPushButton("Paziente Extra");


    esci = new QPushButton("ESCI");

    groupButton = new QGroupBox();

    vbox = new QVBoxLayout;
    vbox->addWidget(text);
    vbox->addWidget(button1);
    vbox->addWidget(button2);

    vbox->addWidget(esci);
    groupButton->setLayout(vbox);

    setStyle();
    menu->addWidget(groupButton);
}

void Gestionale_Tamponi::showTesta()  {
    formTamponi->setVisible(true);
    listaVisite->setVisible(false);
    barraricerca->setVisible(false);
    groupLabel->setVisible(false);
    setFixedSize(900,500);
}

void Gestionale_Tamponi::showListaPazienti() {
    formTamponi->setVisible(false);
    listaVisite->setVisible(true);
    barraricerca->setVisible(true);
    groupLabel->setVisible(true);
    setFixedSize(1600,900);

}

void Gestionale_Tamponi::closeQuestion() {
    domandaChiusura = new QMessageBox;
    domandaChiusura->setText("Vuoi davvero chiudere l'applicazione?");
    domandaChiusura->setStandardButtons(QMessageBox::Yes| QMessageBox::Cancel);
    int ret = domandaChiusura->exec();
    switch(ret){
        case QMessageBox::Yes:
            closeButton();
            break;
        case QMessageBox::No:
            break;
    }
}
void Gestionale_Tamponi::closeButton() {
    this->close();
}


TV_barraRicerca *Gestionale_Tamponi::getbarraricerca() const{
    return barraricerca;
}

TV_listaVisite *Gestionale_Tamponi::getlistavisite() const {
    return listaVisite;
}

T_form_test *Gestionale_Tamponi::getform() const{
    return formTamponi;
}

unsigned int Gestionale_Tamponi::get_indice_visite() const {
    return indice_visite;
}

void Gestionale_Tamponi::set_indice_visite(unsigned int i) {
    indice_visite = i;
}

void Gestionale_Tamponi::setOperatoreSanitario(OperatoreSanitario * tmp) {
    logged_user = tmp->clone();
    QString displayOperatore = ("Operatore Sanitario : \n" + QString::fromStdString(tmp->stringify()));
    text->setText(displayOperatore);
}

OperatoreSanitario *Gestionale_Tamponi::get_logged_user() const {
    return logged_user;
}
void Gestionale_Tamponi::setStyle(){
    QFile file("://resources/style/scelta.css");
    file.open(QFile::ReadOnly);
    QString styleSheet = QLatin1String(file.readAll());
    setStyleSheet(styleSheet);
}
