#include "mainwindow.h"
#include <QFile>

MainWindow::MainWindow(Controller *c, QWidget *parent) : QWidget(parent), controller(c) {
    mainLayout = new QVBoxLayout(this);
   // setMinimumSize(300,300);
    setFixedSize(450,350);
    setWindowTitle("Covid Manager");
    prova = new QLabel ();
    prova->setPixmap(QPixmap("://resources/img/logoCovid.png"));
    prova->setFixedSize(400,300);
    entra = new QPushButton ("Entra");
    entra->setObjectName("entra");
    mainLayout->addWidget(prova);
    mainLayout->addWidget(entra);
    setStyle();

    setLayout(mainLayout);

    connect(entra, SIGNAL(clicked()), this, SIGNAL(signAccedi()));
}
void MainWindow::setStyle(){
    QFile file("://resources/style/scelta.css");
    file.open(QFile::ReadOnly);
    QString styleSheet = QLatin1String(file.readAll());
    setStyleSheet(styleSheet);
}
