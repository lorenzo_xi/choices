#include "view/gestionale_vax.h"
#include <QCloseEvent>

Gestionale_Vax::Gestionale_Vax(Controller *c , QWidget *parent): QWidget(parent), controller(c) {
    setFixedSize(900,800);
    setWindowTitle("Gestione Vaccini");
    mainLayout = new QHBoxLayout(this);

    menu = new QVBoxLayout ();
    contenuto = new QVBoxLayout();
    formSegnalazione = new V_form_segnalazione();

    indice_visite = Algorithm::getIndex(true);
    listaVisite = new TV_listaVisite(c,true,false,"","visite_vax.json","visite_vax",-1);

    layoutLabel = new QHBoxLayout();
    groupLabel = new QGroupBox();

    nome = new QLabel ("Nome");
    nome->setFixedWidth(150);
    nome->setObjectName("nome_v");
    cognome = new QLabel ("Cognome");
    cognome->setFixedWidth(150);
    cognome->setObjectName("cognome_v");
    data_nascita = new QLabel("Data Nascita");
    data_nascita->setFixedWidth(150);
    data_nascita->setObjectName("data_v");
    CF = new QLabel ("Codice Fiscale");
    CF->setFixedWidth(200);
    CF->setObjectName("cf_v");
    tipoVaccino = new QLabel ("Tipo Vaccino");
    tipoVaccino->setFixedWidth(150);
    tipoVaccino->setObjectName("tipo_v");
    layoutLabel->addWidget(nome);
    layoutLabel->addWidget(cognome);
    layoutLabel->addWidget(data_nascita);
    layoutLabel->addWidget(CF);
    layoutLabel->addWidget(tipoVaccino);


    groupLabel->setLayout(layoutLabel);
    groupLabel->setFixedSize(1200,80);


    if(indice_visite < listaVisite->getListaSize() ){
        formVax = new V_form_vaccini(c, this, true, false, indice_visite);
    }else{
        formVax = new V_form_vaccini(c, this, false, true, indice_visite);
    }

    formVaxEmpty = new V_form_vaccini(c,this,false,0);
    barraricerca  = new TV_barraRicerca(c, this,true);


    indietro = new QPushButton("← torna indietro");

    formSegnalazione->setVisible(false);
    formVax->setVisible(false);
    formVaxEmpty->setVisible(false);
    barraricerca->setVisible(false);
    listaVisite->setVisible(false);
    groupLabel->setVisible(false);

    contenuto->addWidget(formSegnalazione);
    contenuto->addWidget(formVax);
    contenuto->addWidget(formVaxEmpty);
    contenuto->addWidget(barraricerca);
    contenuto->addWidget(groupLabel);
    contenuto->addWidget(listaVisite);

    setLayoutWidth();
    addMenuButtons();

    mainLayout->addWidget(widgetMenu);
    mainLayout->addWidget(widgetContenuto);


    setLayout(mainLayout);

    connect(button4,SIGNAL(clicked()), this, SLOT(showSegnalazione()));
    connect(button3,SIGNAL(clicked()), this, SLOT(showPazienteExtra()));
    connect(button1,SIGNAL(clicked()), this, SLOT(showVaccina()));
    connect(button2, SIGNAL(clicked()), this, SLOT (showVisite()));

    connect(esci, SIGNAL(clicked()), this, SLOT(closeQuestion()));
    connect(indietro, SIGNAL(clicked()), this, SIGNAL(signIndietro()));


}

void Gestionale_Vax::setLayoutWidth() {
    widgetMenu = new QWidget();
    widgetContenuto = new QWidget();
    widgetMenu->setFixedWidth(325);
    menu->addWidget(indietro);  //da fare resize;
    widgetContenuto->setLayout(contenuto);
    widgetContenuto->setMinimumWidth(400);
    widgetMenu->setLayout(menu);
}

void Gestionale_Vax::addMenuButtons() {
    text = new QLabel ("");
    text->setObjectName("os");

    button1=  new QPushButton ("Vaccina");
    button2= new QPushButton("Lista Pazienti");
    button3 = new QPushButton("Paziente Extra");
    button4 = new QPushButton ("Segnalazione");
    esci = new QPushButton("ESCI");

    groupButton = new QGroupBox();

    vbox = new QVBoxLayout;
    vbox->addWidget(text);
    vbox->addWidget(button1);
    vbox->addWidget(button2);
    vbox->addWidget(button3);
    vbox->addWidget(button4);
    vbox->addWidget(esci);
    groupButton->setLayout(vbox);


    setStyle();
    menu->addWidget(groupButton);
}

void Gestionale_Vax::showSegnalazione() {
    formSegnalazione->setVisible(true);

  formVax->setVisible(false);
    formVaxEmpty->setVisible(false);
    barraricerca->setVisible(false);
    groupLabel->setVisible(false);
    listaVisite->setVisible(false);
    setFixedSize(900,800);
}
void Gestionale_Vax::showPazienteExtra() {
    formSegnalazione->setVisible(false);
    formVax->setVisible(false);
    formVaxEmpty->setVisible(true);

    barraricerca->setVisible(false);
    groupLabel->setVisible(false);
    listaVisite->setVisible(false);
    setFixedSize(900,800);
}
void Gestionale_Vax::showVaccina() {
    formSegnalazione->setVisible(false);

    formVax->setVisible(true);
    formVaxEmpty->setVisible(false);
    barraricerca->setVisible(false);
    groupLabel->setVisible(false);
    formVaxEmpty->setVisible(false);
    setFixedSize(900,800);
    listaVisite->setVisible(false);
}
void Gestionale_Vax::showVisite() {
    formSegnalazione->setVisible(false);

    formVax->setVisible(false);
    formVaxEmpty->setVisible(false);
    barraricerca->setVisible(true);
    groupLabel->setVisible(true);
    listaVisite->setVisible(true);
    setFixedSize(1580,900);
}
void Gestionale_Vax::closeQuestion() {
    domandaChiusura = new QMessageBox;
    domandaChiusura->setText("Vuoi davvero chiudere l'applicazione?");
    domandaChiusura->setStandardButtons(QMessageBox::Yes| QMessageBox::Cancel);
    int ret = domandaChiusura->exec();
    switch(ret){
        case QMessageBox::Yes:
            closeButton();
            break;
        case QMessageBox::No:
            break;
    }
}

void Gestionale_Vax::closeButton() {
    this->close();
}



TV_barraRicerca *Gestionale_Vax::getBarraRicerca() const{
    return barraricerca;
}

TV_listaVisite *Gestionale_Vax::getlistavisite() const {
    return listaVisite;
}
unsigned int Gestionale_Vax::get_indice_visite() const {
    return indice_visite;
}

void Gestionale_Vax::set_indice_visite(unsigned int i) {
    indice_visite = i;
}

void Gestionale_Vax::setOperatoreSanitario(OperatoreSanitario * tmp) {
    logged_user = tmp->clone();
    QString displayOperatore = ("Operatore Sanitario : \n" + QString::fromStdString(tmp->stringify()));
    text->setText(displayOperatore);
}
V_form_vaccini *Gestionale_Vax::getform() const{
    return formVax;
}

OperatoreSanitario* Gestionale_Vax::get_logged_user() const {
    return logged_user;
}

V_form_vaccini *Gestionale_Vax::getEmptyForm() const {
    return formVaxEmpty;
}
void Gestionale_Vax::setStyle(){
    QFile file("://resources/style/scelta.css");
    file.open(QFile::ReadOnly);
    QString styleSheet = QLatin1String(file.readAll());
    setStyleSheet(styleSheet);
}
