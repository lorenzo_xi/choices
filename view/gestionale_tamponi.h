#ifndef COVIDMANAGER_GESTIONALE_TAMPONI_H
#define COVIDMANAGER_GESTIONALE_TAMPONI_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QLabel>
#include <QGroupBox>
#include <iostream>
#include <QBoxLayout>
#include <QMessageBox>
#include <view/widget/TV_barraRicerca.h>
#include "view/widget/V_form_segnalazione.h"
#include "view/widget/TV_listaVisite.h"
#include "view/widget/T_form_test.h"


class Controller;


class Gestionale_Tamponi: public QWidget {
    Q_OBJECT;
public:
    Gestionale_Tamponi(Controller* controller= nullptr, QWidget *parent = nullptr);
    ~Gestionale_Tamponi() = default;
    void setStyle();

    TV_barraRicerca *getbarraricerca() const;
    TV_listaVisite *getlistavisite() const;
    T_form_test *getform() const;
    unsigned int get_indice_visite() const;
    OperatoreSanitario* get_logged_user() const;

    void set_indice_visite(unsigned int);
    void setOperatoreSanitario(OperatoreSanitario*);


public slots:
    void showTesta();
    void showListaPazienti();
    void closeButton();
    void closeQuestion();


private:
    Controller      *controller;

    OperatoreSanitario* logged_user;

    QWidget         *widgetMenu, *widgetContenuto;

    QHBoxLayout     *mainLayout, *layoutLabel;

    QPushButton     *button1, *button2, *button3, *esci, *indietro;

    QGroupBox       *groupButton, *groupLabel;

    QVBoxLayout     *contenuto, *menu, *vbox;

    QLabel          *text, *prova, *nome, *n, *data_nascita, *cognome, *email, *recapito, *tipo, *CF, *esito;

    T_form_test     *formTamponi;

    TV_barraRicerca*barraricerca;

    TV_listaVisite *listaVisite;

    QMessageBox *domandaChiusura;

    unsigned int indice_visite;

    void setLayoutWidth();
    void addMenuButtons();
    void addLabelScrollArea();

    signals:
    void signIndietro();
};


#endif //COVIDMANAGER_GESTIONALE_TAMPONI_H
