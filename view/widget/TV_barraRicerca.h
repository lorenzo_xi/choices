#ifndef COVIDMANAGER_T_BARRARICERCA_H
#define COVIDMANAGER_T_BARRARICERCA_H

#include <QWidget>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QRadioButton>
#include <QPushButton>
#include <QGroupBox>
#include <QCheckBox>
#include "iostream"

class Controller;

class TV_barraRicerca : public QWidget{
    Q_OBJECT

public:

    TV_barraRicerca(Controller* = nullptr, QWidget * = nullptr, bool =true);
    ~TV_barraRicerca() = default;

public slots:
    /**
     * @brief   gestisce l'abilitazione/disabilitazione del widget QLineEdit 'inputfield' in base al check del radiobutton 'insertcf'
     */
    void manage_cf();

    /**
     * @brief   emette, in base ai parametri presi dai widget, il segnale send_query(bool, bool, QString, int)
     */
    void send();


signals:
    /**
     * @brief   segnale inviato al controller che invia la query in base ai parametri di ricerca della barra di ricerca
     * @par1    flag booleano che indica se la query è relativa a vaccini o test/sierologico
     * @par2    flag booleano che indica se la query ricerca visite già erogate o meno
     * @par3    flag QString che indica che la query coinvolge anche il parametro (nel caso sia valorizzato) o meno (nel caso sia "")
     * @par4    flag int che indica che la query ricerca visite con un determinato esito (0 negativo, 1 positivo, -1 in elaborazione)
     */
    void send_query(bool, bool, QString, int);

private:

    Controller      *controller;

    QHBoxLayout     *mainLayout;
    QGridLayout     *gridLayout;

    QLineEdit       *inputfield;
    QCheckBox       *insertcf;

    QRadioButton    *b_erogate,*b_nonerogate;

    QPushButton     *search;

    QGroupBox       *groupBasicSearch;
    QHBoxLayout     *hbox1;
    bool            vaxortest;


    /**
     * @brief   aggiunge gli elementi di ricerca alla barra di ricerca
     * @par1    flag booleano in base al quale aggiunge (==true) o meno i parametri di ricerca relativi ai test
     */
    void addSearchElements();


    /**
     *  @brief  connette widget, slot, sengali
     */
    void addConnections();
};

#endif //COVIDMANAGER_T_BARRARICERCA_H
