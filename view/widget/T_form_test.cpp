#include "T_form_test.h"

T_form_test::T_form_test(Controller * controller, QWidget * parent, bool dataLoading, unsigned int index) {
    mainlayout = new QVBoxLayout(this);
    Vector<QString>* v=new Vector<QString>;
    data_err_popup = new QMessageBox(QMessageBox::Icon::Warning,"Errore","");
    data_err_popup->setStandardButtons(QMessageBox::Close);

    QUrl path("resources/jsondata/visite_test.json");

    try {
        if(dataLoading){
            Algorithm::Test_readFile(*v, path.path().toStdString(), index);
            addLabelsAndLineEdits(dataLoading, v->at(0),v->at(1),v->at(2));
            addRadioButtons(dataLoading, v->at(3)=="sierologico"?2:v->at(3).toInt());
            addActionsButtons();
            addConnections();
            finish = nullptr;
        }else{
            addFinishLabel();
        }
    } catch (std::out_of_range) {
        throw std::out_of_range("!");
    }

}

void T_form_test::addLabelsAndLineEdits(bool isDataLoaded, QString codicefiscale, QString recapitoemail, QString recapitoloTelefono) {

    layoutData = new QGridLayout();
    groupData = new QGroupBox("Dati");

    cf = new QLabel("CF: ");
    cfEdit = new QLineEdit();
    cfEdit->setText(codicefiscale);
    cfEdit->setValidator(new QRegularExpressionValidator(QRegularExpression(("[A-Z][A-Z][A-Z][A-Z][A-Z][A-Z][0-9][0-9][A-Z][0-9][0-9][A-Z][0-9][0-9][0-9][A-Z]"))));

    email = new QLabel("Email: ");
    emailEdit = new QLineEdit();
    emailEdit->setText(recapitoemail);
    emailEdit->setValidator(new QRegularExpressionValidator(QRegularExpression ("[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}")));

    recapito = new QLabel("Recapito: ");
    telEdit = new QLineEdit();
    telEdit->setText(recapitoloTelefono);   //formato telefono (le parentesi {} messe solo per chiarezza):  {3cifre}-{3/4cifre}-{4/8cifre}
    telEdit->setValidator(new QRegularExpressionValidator (QRegularExpression("([[:digit:]]{3}-[[:digit:]]{3,4}-[[:digit:]]{4,8})")));

    groupData->setEnabled(false);

    layoutData->addWidget(cf, 1, 1);
    layoutData->addWidget(cfEdit, 1, 2);

    layoutData->addWidget(email, 2, 1);
    layoutData->addWidget(emailEdit, 2, 2);

    layoutData->addWidget(recapito, 3, 1);
    layoutData->addWidget(telEdit, 3, 2);

    groupData->setLayout(layoutData);

    mainlayout->addWidget(groupData);
}

void T_form_test::addRadioButtons(bool isDataLoaded, int option) {
    layoutButtons = new QHBoxLayout();
    groupButtons = new QGroupBox("Seleziona tipologia:");

    antigenico = new QRadioButton("Antigenico");
    molecolare = new QRadioButton("Molecolare");
    sierologico= new QRadioButton("Sierologico");

    if(isDataLoaded){
        switch (option) {
            case 0: antigenico->setChecked(true);
                    break;
            case 1: molecolare->setChecked(true);
                    break;
            case 2: sierologico->setChecked(true);
                    break;
            default:antigenico->setChecked(true);
                    break;
        }
        groupButtons->setDisabled(true);
    }

    layoutButtons->addWidget(antigenico);
    layoutButtons->addWidget(molecolare);
    layoutButtons->addWidget(sierologico);

    groupButtons->setLayout(layoutButtons);

    mainlayout->addWidget(groupButtons);
}

void T_form_test::addActionsButtons() {

    update = new QPushButton("SALVA DATI");
    update->setVisible(false);
    mainlayout->addWidget(update);

    edit = new QPushButton("EDIT");
    mainlayout->addWidget(edit);

    somministra = new QPushButton("VISITA EFFETTUATA");
    somministra->setEnabled(true);
    mainlayout->addWidget(somministra);

    stampa = new QPushButton("STAMPA RICEVUTA");
    stampa->setVisible(false);
    mainlayout->addWidget(stampa);

    ok = new QPushButton("PROSSIMO PAZIENTE");
    ok->setDisabled(true);
    mainlayout->addWidget(ok);
}

void T_form_test::next_visita(int indice){


    if(indice>=0){
        try{
            Vector<QString>* v=new Vector<QString>;
            QUrl path("resources/jsondata/visite_test.json");
            Algorithm::Test_readFile(*v, path.path().toStdString(), indice);

            cfEdit->setText(v->at(0));
            emailEdit->setText(v->at(1));
            telEdit->setText(v->at(2));

            switch (v->at(3).toInt()) {
                case 0: antigenico->setChecked(true);
                    break;
                case 1: molecolare->setChecked(true);
                    break;
                case 2: sierologico->setChecked(true);
                    break;
                default:antigenico->setChecked(true);
                    break;
            }

        } catch(std::out_of_range){

            groupButtons->setVisible(false);
            ok->setVisible(false);
            edit->setVisible(false);
            somministra->setVisible(false);
            groupData->setVisible(false);
            stampa->setVisible(false);

        }
    }

}

void T_form_test::addFinishLabel(){

    groupFinish = new QGroupBox();
    layoutFinish = new QHBoxLayout();
    finish = new QLabel("PER OGGI PAZIENTI FINITI");
    finish->setObjectName("finish_t");
    finish->setAlignment(Qt::AlignCenter);
    layoutFinish->addWidget(finish);
    groupFinish->setLayout(layoutFinish);
    mainlayout->addWidget(groupFinish);
}


void T_form_test::addConnections() {
    connect(ok,SIGNAL(clicked()), this, SLOT(send_next()));
    connect(edit,SIGNAL(clicked()), this, SLOT(edit_form()));
    connect(update,SIGNAL(clicked()), this, SLOT(update_data()));
    connect(somministra,SIGNAL(clicked()), this, SLOT(manage_ok_button()));
    connect(stampa,SIGNAL(clicked()), this, SLOT(manage_stampa()));
}

void T_form_test::edit_form() {
    groupData->setEnabled(! groupData->isEnabled());
    edit->setVisible(false);
    somministra->setEnabled(false);
    update->setVisible(true);
}

void T_form_test::update_data() {
    edit->setVisible(true);
    update->setVisible(false);
    groupData->setEnabled(false);
    somministra->setEnabled(true);
    if(dataValidation()){
        emit aggiorna_paziente(cfEdit->text(),emailEdit->text(),telEdit->text());
    }
}

void T_form_test::manage_ok_button() {
    ok->setEnabled(true);
    edit->setEnabled(true);
    somministra->setEnabled(false);
    stampa->setVisible(true);
}

void T_form_test::manage_stampa() {

    QString nomefile = "resources/pdf/test/visita" + cfEdit->text() +  QDateTime::currentDateTime().toString("dd.MM.yyyy");
    QString fileName = QFileDialog::getSaveFileName(this, "Esporta il PDF e stampalo", nomefile+".pdf", "*.pdf",nullptr, QFileDialog::DontUseNativeDialog);
    if (QFileInfo(fileName).suffix().isEmpty()) { fileName.append(".pdf"); }
    QPrinter printer(QPrinter::PrinterResolution);
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setOutputFileName(fileName);
    QTextDocument doc;
    doc.setHtml(Algorithm::get_testoPdf_test(1,cfEdit->text(),emailEdit->text(),telEdit->text()));
    doc.print(&printer);
    QDesktopServices::openUrl(QUrl(nomefile+".pdf"));
}

void T_form_test::send_next() {
    ok->setEnabled(true);
    somministra->setEnabled(true);
    stampa->setVisible(false);
    emit paziente_successivo();
}

bool T_form_test::dataValidation(){
    int n = 0;
    QString cf_to_validate = cfEdit->text();
    QString em_to_validate = emailEdit->text();
    QString te_to_validate = telEdit->text();
    bool all_is_validate=false;

    if(cfEdit->validator()->validate(cf_to_validate,n) == QValidator::Acceptable
        &&
        emailEdit->validator()->validate(em_to_validate,n) == QValidator::Acceptable
        &&
            telEdit->validator()->validate(te_to_validate,n) == QValidator::Acceptable){

            all_is_validate = true;
        }else{

            QString err="Errore nei seguenti campi dati: \n";
            if(cfEdit->validator()->validate(cf_to_validate,n) != QValidator::Acceptable ){
                err.append("codice fiscale");
                cfEdit->clear();
            }
            if(emailEdit->validator()->validate(em_to_validate,n) != QValidator::Acceptable){
                err.append(err.isEmpty()? "email":", email");
                emailEdit->clear();
            }
            if(telEdit->validator()->validate(te_to_validate,n) != QValidator::Acceptable){
                err.append(err.isEmpty()? "telefono":", telefono");
                telEdit->clear();
            }
            data_err_popup->setText(err);
            data_err_popup->setMinimumWidth(1000);
            data_err_popup->show();
            ok->setEnabled(false);
        }

        return all_is_validate;
    }
