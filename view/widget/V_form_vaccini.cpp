#include "V_form_vaccini.h"

V_form_vaccini::V_form_vaccini(Controller * controller, QWidget * parent, bool dataLoading, bool finish, unsigned int index) {

    mainlayout = new QVBoxLayout(this);
    Vector<QString>* v=new Vector<QString>;

    QUrl path("resources/jsondata/visite_vax.json");
    data_err_popup = new QMessageBox(QMessageBox::Icon::Warning,"Errore","");
    data_err_popup->setStandardButtons(QMessageBox::Close);


    if(dataLoading){
        Algorithm::Vax_readFile(*v, path.path().toStdString(), index);
        addLabelsAndLineEdits(dataLoading, v->at(0),v->at(1),v->at(2), v->at(3), v->at(4), v->at(5));
        addRadioButtonsSesso(dataLoading, v->at(6));
        addComboBoxVax(dataLoading, v->at(7));
        addCartellaClinica(dataLoading, v->at(8));
    }else{
        if(!finish){
            addLabelsAndLineEdits(dataLoading, "","","", "", "", "");
            addRadioButtonsSesso(dataLoading, "");
            addCartellaClinica(dataLoading, "", 1);
            addComboBoxVax(dataLoading, "");

        }else{
            addFinishLabel();
        }
    }

    addActionsButtons();
    addConnections(dataLoading);

}

void V_form_vaccini::addLabelsAndLineEdits(bool isDataLoaded, QString nomeL, QString cognomeL, QString codicefiscale, QString recapitoemail, QString recapitoloTelefono, QString data) {

    layoutData = new QGridLayout();
    layoutData->setAlignment(Qt::AlignLeft);
    groupData = new QGroupBox("Dati");

    nome = new QLabel("Nome: ");
    nomeEdit = new QLineEdit();
    nomeEdit->setText(nomeL);
    nomeEdit->setValidator(new QRegularExpressionValidator(QRegularExpression(("[A-Za-z]{1,255}"))));

    cognome= new QLabel("Cognome: ");
    cognomeEdit = new QLineEdit();
    cognomeEdit->setText(cognomeL);
    cognomeEdit->setValidator(new QRegularExpressionValidator(QRegularExpression(("[A-Za-z]{1,255}"))));

    CF = new QLabel("CF: ");
    CFEdit = new QLineEdit();
    CFEdit->setText(codicefiscale);
    CFEdit->setValidator(new QRegularExpressionValidator(QRegularExpression(("[A-Z][A-Z][A-Z][A-Z][A-Z][A-Z][0-9][0-9][A-Z][0-9][0-9][A-Z][0-9][0-9][0-9][A-Z]"))));

    email = new QLabel("Email: ");
    emailEdit = new QLineEdit();
    emailEdit->setText(recapitoemail);
    emailEdit->setValidator(new QRegularExpressionValidator ( QRegularExpression ("^[0-9a-zA-Z.]+@[0-9a-zA-Z]+([-.][0-9a-zA-Z]+)([0-9a-zA-Z][.])[a-zA-Z]{2,6}$")));


    telefono = new QLabel("Recapito: ");
    telEdit = new QLineEdit();
    telEdit->setText(recapitoloTelefono);
    telEdit->setValidator(new QRegularExpressionValidator (QRegularExpression("([[:digit:]]{3}-[[:digit:]]{3,4}-[[:digit:]]{4,8})")));

    dataNascita= new QLabel("Data Nascita");
    dataNEdit = new QLineEdit();
    qDateEdit = new QDateEdit;
    qDateEdit->setDisplayFormat("dd/MM/yyyy");
    qDateEdit->setDate(QDate::fromString(data, "dd/MM/yyyy"));
    qDateEdit->setMinimumDate(QDate(1900, 01, 01));
    qDateEdit->setMaximumDate(QDate(2021,01,01));

    if(isDataLoaded){
        groupData->setEnabled(false);
    }else{
        groupData->setEnabled(false);
        nomeEdit->setPlaceholderText("Scrivi qui il nome...");
        cognomeEdit->setPlaceholderText("Scrivi qui il cognome...");
        CFEdit->setPlaceholderText("Scrivi qui il il codice fiscale...");
        emailEdit->setPlaceholderText("Scrivi qui l'email...");
        telEdit->setPlaceholderText("Scrivi qui il recapito telefonico...");
    }

    layoutData->addWidget(nome, 1, 1);
    layoutData->addWidget(nomeEdit, 1, 2);

    layoutData->addWidget(cognome, 2, 1 );
    layoutData->addWidget(cognomeEdit, 2, 2);

    layoutData->addWidget(CF, 3, 1);
    layoutData->addWidget(CFEdit, 3, 2);

    layoutData->addWidget(email, 4, 1);
    layoutData->addWidget(emailEdit, 4, 2);

    layoutData->addWidget(telefono, 5, 1);
    layoutData->addWidget(telEdit, 5, 2);

    layoutData->addWidget(dataNascita, 6, 1);
    layoutData->addWidget(qDateEdit, 6, 2);

    groupData->setMinimumSize(250,300);
    groupData->setLayout(layoutData);

    mainlayout->addWidget(groupData);
}

void V_form_vaccini::addRadioButtonsSesso(bool isDataLoaded, QString option) {
    layoutButtons = new QHBoxLayout();
    groupButtons = new QGroupBox("Sesso:");

    maschio = new QRadioButton("Maschio");
    femmina = new QRadioButton("Femmina");
    nobinary= new QRadioButton("Altro");

    if(isDataLoaded){
        groupButtons->setEnabled(false);

      if(option=="M"){
          maschio->setChecked(true);
      }
      else{
          femmina->setChecked(true);
      }
        groupButtons->setDisabled(true);
    }else{
        groupButtons->setEnabled(false);
        maschio->setChecked(true);
    }

    layoutButtons->addWidget(maschio);
    layoutButtons->addWidget(femmina);

    groupButtons->setLayout(layoutButtons);

    mainlayout->addWidget(groupButtons);

}
void V_form_vaccini::addComboBoxVax(bool isDataLoaded, QString s) {
    layoutComboBox = new QHBoxLayout();
    groupComboBox = new QGroupBox("Seleziona Vaccino da somministrare: ");

    menuVax = new QComboBox();
    menuVax->addItem("Pfizer");
    menuVax->addItem("Moderna");
    menuVax->addItem("AstraZeneca");
    menuVax->addItem("JandJ");
    menuVax->addItem("NovaVax");
    menuVax->addItem("SputnikV");
    menuVax->addItem("SinoVac");
    menuVax->addItem("Non specificato");

    if(isDataLoaded){
        menuVax->setEnabled(false);
        if(s == "Pfizer"){
            menuVax->setCurrentIndex(0);
        }else if(s == "Moderna"){
            menuVax->setCurrentIndex(1);

        }else if(s == "Astrazeneca"){
            menuVax->setCurrentIndex(2);

        }else if(s == "JandJ"){
            menuVax->setCurrentIndex(3);

        }else if(s == "Novavax"){
            menuVax->setCurrentIndex(4);

        }else if(s == "SputnikV"){
            menuVax->setCurrentIndex(5);

        }else if(s == "SinoVac"){
            menuVax->setCurrentIndex(6);

        }else{
            //default
            menuVax->setCurrentIndex(2);
        }
    }else{
        menuVax->setEnabled(true);
        menuVax->setCurrentIndex(2);
    }

    layoutComboBox->addWidget(menuVax);
    groupComboBox->setLayout(layoutComboBox);
    mainlayout->addWidget(groupComboBox);
}
void V_form_vaccini::addCartellaClinica(bool isDataLoaded, QString patologieL, int option) {
    layoutCartella = new QGridLayout();
    groupCartella = new QGroupBox ("Cartella clinica");

    patologie = new QLabel("Patologie: ");
    patologieEdit = new QSpinBox ();
    patologieEdit->setRange(0,25);
    patologieEdit->setSingleStep(1);
    QStringList stringList = patologieL.split(QLatin1Char(','));

    rischioCalc = new QLabel ("Rischio calcolato: ");

    hadCovid = new QLabel ("Ha contratto il covid?");
    covidYes = new QRadioButton("Si");
    covidNo = new QRadioButton("No");

    rischioEdit = new QLabel();
    rischioEdit->setEnabled(false);
    layoutCartella->addWidget(rischioEdit, 2, 2);


    if (isDataLoaded){
        patologieEdit->setValue(stringList[0].toInt());
        groupCartella->setEnabled(false);
        if(stringList[2]=="1"){
            covidYes->setChecked(true);
        }
        else{
            covidNo->setChecked(true);
        }
        rischioEdit->setText(QString::fromStdString(Algorithm::fromEnumRiskToStdString(static_cast<EnumTypes::Risk>(stringList[1].toInt()))));
    }
    else{
        if (option==1){
            covidYes->setChecked(true);
        }
        else{
            covidYes->setChecked(false);
        }
        rischioEdit = new QLabel();
        rischioEdit->setText("Non ancora calcolato");
        rischioEdit->setEnabled(false);
        layoutCartella->addWidget(rischioEdit, 2, 2);
        groupCartella->setEnabled(false);
    }


    layoutCartella->addWidget(patologie, 1, 1);
    layoutCartella->addWidget(patologieEdit, 1, 2);
    layoutCartella->addWidget(rischioCalc, 2, 1);
    layoutCartella->addWidget(hadCovid, 3, 1);
    layoutCartella->addWidget(covidYes, 3, 2);
    layoutCartella->addWidget(covidNo, 3, 3 );

    groupCartella->setLayout(layoutCartella);
    groupCartella->setEnabled(false);
    mainlayout->addWidget(groupCartella);


}
void V_form_vaccini::addActionsButtons() {

    update = new QPushButton("SALVA DATI");
    update->setVisible(false);
    mainlayout->addWidget(update);

    edit = new QPushButton("EDIT");
    mainlayout->addWidget(edit);

    somministra = new QPushButton("VISITA EFFETTUATA");
    somministra->setEnabled(true);
    mainlayout->addWidget(somministra);

    calcolaRischio = new QPushButton("CALCOLA IL RISCHIO");
    calcolaRischio->setEnabled(false);
    mainlayout->addWidget(calcolaRischio);

    stampa = new QPushButton("STAMPA RICEVUTA");
    stampa->setVisible(false);
    mainlayout->addWidget(stampa);

    ok = new QPushButton("PROSSIMO PAZIENTE");
    ok->setEnabled(false);
    mainlayout->addWidget(ok);

    clear = new QPushButton("PULISCI FORM");
    clear->setEnabled(false);
}

void V_form_vaccini::disableEditButton() {
    if(edit){
        edit->setEnabled(false);
    }
}

void V_form_vaccini::next_visita(int indice){
    if(indice>=0){
        try{
            Vector<QString>* v=new Vector<QString>;
            QUrl path("resources/jsondata/visite_vax.json");
            Algorithm::Vax_readFile(*v, path.path().toStdString(), indice);


            nomeEdit->setText(v->at(0));
            cognomeEdit->setText(v->at(1));
            CFEdit->setText(v->at(2));
            emailEdit->setText(v->at(3));
            telEdit->setText(v->at(4));

            Data d = v->at(5).toStdString();    //utilizza costruttore Data(std::string)

            QDate tmp_date;
            tmp_date.setDate(d.getAnno(),d.getMese(),d.getGiorno());
            qDateEdit->setDate(tmp_date);

            if(v->at(6)=="M"){
                maschio->setChecked(true);
            }
            else{
                femmina->setChecked(true);
            }

            QString s = v->at(7);

            if(s == "Pfizer"){
                menuVax->setCurrentIndex(0);
            }else if(s == "Moderna"){
                menuVax->setCurrentIndex(1);

            }else if(s == "Astrazeneca"){
                menuVax->setCurrentIndex(2);

            }else if(s == "JandJ"){
                menuVax->setCurrentIndex(3);

            }else if(s == "Novavax"){
                menuVax->setCurrentIndex(4);

            }else if(s == "SputnikV"){
                menuVax->setCurrentIndex(5);

            }else if(s == "SinoVac"){
                menuVax->setCurrentIndex(6);

            }else{
                //default
                menuVax->setCurrentIndex(2);
            }

            QStringList stringList2 = v->at(8).split(QLatin1Char(','));
            patologieEdit->setValue(stringList2[0].toInt());
            rischioEdit->setText(QString::fromStdString(Algorithm::fromEnumRiskToStdString(static_cast<EnumTypes::Risk>(stringList2[1].toInt()))));
            if(stringList2[2]=="1"){
                covidYes->setChecked(true);
            }
            else{
                covidNo->setChecked(true);
            }


        } catch(std::out_of_range){

            groupButtons->setVisible(false);
            ok->setVisible(false);
            edit->setVisible(false);
            somministra->setVisible(false);
            groupData->setVisible(false);
            stampa->setVisible(false);
            mainlayout->addWidget(new QLabel("PAZIENTI FINITI PER OGGI"));
        }
    }

}


void V_form_vaccini::addFinishLabel() {
    groupFinish = new QGroupBox();
    layoutFinish = new QHBoxLayout();
    finish = new QLabel("PER OGGI ABBIAMO FINITO");
    layoutFinish->addWidget(finish);
    groupFinish->setLayout(layoutFinish);
    mainlayout->addWidget(groupFinish);
}



void V_form_vaccini::addConnections(bool flagDataLoading) {
    if(ok){
        if(flagDataLoading){
            connect(ok,SIGNAL(clicked()), this, SLOT(send_next()));
        }else{
            connect(ok,SIGNAL(clicked()), this, SLOT(salva_visita_extra()));
        }
    }
    connect(edit,SIGNAL(clicked()), this, SLOT(edit_form()));
    connect(update,SIGNAL(clicked()), this, SLOT(update_data()));
    connect(somministra,SIGNAL(clicked()), this, SLOT(manage_ok_button()));
    connect(stampa,SIGNAL(clicked()), this, SLOT(manage_stampa()));
    connect(clear,SIGNAL(clicked()),this, SLOT(clear_form()));
    connect(calcolaRischio,SIGNAL(clicked()), this, SLOT(calcoloRischio()));

}

void V_form_vaccini::edit_form() {
    groupData->setEnabled(! groupData->isEnabled());
    groupButtons->setEnabled(!groupButtons->isEnabled());
    groupCartella->setEnabled(!groupCartella->isEnabled());
    edit->setVisible(false);
    calcolaRischio->setEnabled(true);
    somministra->setEnabled(false);
    update->setVisible(true);
}

void V_form_vaccini::update_data() {
    edit->setVisible(true);
    update->setVisible(false);
    groupData->setEnabled(false);
    groupCartella->setEnabled(false);
    groupComboBox->setEnabled(false);
    groupButtons->setEnabled(false);
    somministra->setEnabled(true);
    calcolaRischio->setEnabled(false);
    QString sesso = (maschio->isChecked()?"M":"F");
    HealthData hd ( patologieEdit->text().toInt(),
                Algorithm::fromStdStringToEnumRisk(rischioEdit ? (rischioEdit->text()).toStdString(): "Basso"),
                covidYes->isChecked(),
                true);

    Data dn = (qDateEdit->text().toStdString());

    if(dataValidation()) {
        emit aggiorna_paziente(nomeEdit->text(), cognomeEdit->text(), CFEdit->text(), emailEdit->text(),
                               telEdit->text(), sesso, dn, hd);
    }
}

void V_form_vaccini::manage_ok_button() {
    ok->setEnabled(true);
    edit->setEnabled(true);
    calcolaRischio->setEnabled(false);
    somministra->setEnabled(false);
    stampa->setVisible(true);
}

void V_form_vaccini::manage_stampa() {

    QString nomefile = "resources/pdf/vaccini/visita" + CFEdit->text() +  QDateTime::currentDateTime().toString("dd.MM.yyyy");
    QString fileName = QFileDialog::getSaveFileName(this, "Esporta PDF e stampalo", nomefile+".pdf", "*.pdf",nullptr, QFileDialog::DontUseNativeDialog);
    if (QFileInfo(fileName).suffix().isEmpty()) { fileName.append(".pdf"); }
    QPrinter printer(QPrinter::PrinterResolution);
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setOutputFileName(fileName);
    QTextDocument doc;
    doc.setHtml(Algorithm::get_testoPdf_vax(nomeEdit->text(), cognomeEdit->text(), dataNEdit->text(), CFEdit->text(),emailEdit->text(),telEdit->text(),menuVax->itemText(menuVax->currentIndex())));
    doc.print(&printer);
    QDesktopServices::openUrl(QUrl(nomefile+".pdf"));
}

void V_form_vaccini::send_next() {
    ok->setEnabled(false);
    somministra->setEnabled(true);
    stampa->setVisible(false);
    emit paziente_successivo();
}

bool V_form_vaccini::dataValidation(){
    int n=0;
    QString nm_to_validate = nomeEdit->text();
    QString cg_to_validate = cognomeEdit->text();
    QString cf_to_validate = CFEdit->text();
    QString em_to_validate = emailEdit->text();
    QString te_to_validate = telEdit->text();
    QString pat_to_validate = patologieEdit->text();
    bool all_is_validate=false;

    if(CFEdit->validator()->validate(cf_to_validate,n) == QValidator::Acceptable
       &&
       telEdit->validator()->validate(te_to_validate,n) == QValidator::Acceptable
       &&
       nomeEdit->validator()->validate(nm_to_validate,n) == QValidator::Acceptable
       &&
       cognomeEdit->validator()->validate(cg_to_validate,n) == QValidator::Acceptable
       ){

        all_is_validate = true;
    }else{

        QString err="Errore nei seguenti campi dati: \n";

        if(CFEdit->validator()->validate(cf_to_validate,n) != QValidator::Acceptable ){
            err.append("codice fiscale");
            CFEdit->clear();
        }
        if(telEdit->validator()->validate(te_to_validate,n) != QValidator::Acceptable){
            err.append(err.isEmpty()? "telefono":", telefono");
            telEdit->clear();
        }
        if(nomeEdit->validator()->validate(nm_to_validate,n) != QValidator::Acceptable){
            err.append(err.isEmpty()? "nome":", nome");
            nomeEdit->clear();
        }
        if(cognomeEdit->validator()->validate(cg_to_validate,n) != QValidator::Acceptable){
            err.append(err.isEmpty()? "cognome":", cognome");
            cognomeEdit->clear();
        }
        data_err_popup->setText(err);
        data_err_popup->setMinimumWidth(1000);
        data_err_popup->show();
        ok->setEnabled(false);
        layoutComboBox->setEnabled(true);
        groupComboBox->setEnabled(true);
        menuVax->setEnabled(true);
    }
    return all_is_validate;
}

void V_form_vaccini::salva_visita_extra() {

    if(dataValidation()) {

        stampa->setVisible(true);
        stampa->setEnabled(true);

        Data dn = (qDateEdit->text().toStdString());

        Utente u(nomeEdit->text().toStdString(),dn,
                 cognomeEdit->text().toStdString(),
                 (maschio->isChecked() ? "M" : ((femmina->isChecked() ? "F" : "A"))),
                 CFEdit->text().toStdString());

        Paziente p(u,
                   emailEdit->text().toStdString(),
                   telEdit->text().toStdString(),
                   HealthData(patologieEdit->text().toInt(),
                              static_cast<EnumTypes::Risk>(rischioCalc->text().toInt()),
                              covidYes->isChecked(),
                              true)
        );

        Evento e;
        Data d = QDateTime::currentDateTime().date().toString("dd-MM-yyyy").toStdString();
        Visita v(e, d, false, false, true, p,OperatoreSanitario());

        EnumTypes::Vax tipovax = (Algorithm::fromStringToEnum(menuVax->currentText().toStdString()));
        Vaccinazione vax(v, tipovax);
        emit send_visita_extra(vax);
        clear_form();
        layoutComboBox->setEnabled(true);
        groupComboBox->setEnabled(true);
        menuVax->setEnabled(true);
        menuVax->setEditable(false);

    }
}

void V_form_vaccini::clear_form() {

    clear->setVisible(false);
    stampa->setVisible(false);

    nomeEdit->setText("");
    cognomeEdit->setText("");
    CFEdit->setText("");
    emailEdit->setText("");
    telEdit->setText("");
    patologieEdit->clear();
    qDateEdit->clear();
    maschio->setChecked(false);
    femmina->setChecked(false);
    covidYes->setChecked(false);
    covidNo->setChecked(false);
    rischioCalc->setText("");
}
void V_form_vaccini::calcoloRischio() {
    int num =patologieEdit->text().toInt();
    int rischio = 0;
    EnumTypes::Risk tmp =EnumTypes::Basso;

    if(num == 1){
        rischio+=5;
    }else if(num == 2){
        rischio+=30;
    }else if (num > 2){
        rischio+=50;
    }

    Data dn = qDateEdit->text().toStdString();
    rischio+=dn.getAnno()-2021;
    if(rischio < 20){
        tmp=EnumTypes::Basso;
    }else if(rischio>=20 && rischio <= 50){
        tmp=EnumTypes::Medio;
    }else{
        tmp=EnumTypes::Alto;
    }
    rischioEdit->setText(QString::fromStdString(Algorithm::fromEnumRiskToStdString(tmp)));
}
