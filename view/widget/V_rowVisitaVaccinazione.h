#ifndef COVIDMANAGER_V_ROWVISITAVACCINAZIONE_H
#define COVIDMANAGER_V_ROWVISITAVACCINAZIONE_H

#include <QDebug>
#include <QWidget>
#include <QHBoxLayout>
#include <QGroupBox>
#include <QLabel>
#include "model/Vector.h"

class Controller;

class V_rowVisitaVaccinazione: public QWidget {

public:

    V_rowVisitaVaccinazione(Controller * = nullptr, QWidget * = nullptr, Vector<QString> = Vector<QString>());
    ~V_rowVisitaVaccinazione() = default;

public slots:

private:

    QHBoxLayout     *mainLayout;

    QLabel *nome;
    QLabel *cognome;
    QLabel *data_nascita;
    QLabel *c_f;
    QLabel *richiamo;
    QLabel *tipoVaccino;
    QLabel *statoVisita; //effettuata o no

    void addLabels(const Vector<QString>&);
};

#endif //COVIDMANAGER_V_ROWVISITAVACCINAZIONE_H