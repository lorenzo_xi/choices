#include "T_rowVisitaTest.h"

T_rowVisitaTest::T_rowVisitaTest(Controller * controller, QWidget * parent, const Vector<QString>& vct, bool tamponeorsierologico) {

    mainLayout = new QGridLayout(this);
    setMinimumHeight(75);
    data_err_popup = new QMessageBox(QMessageBox::Icon::Warning,"Errore","");
    data_err_popup->setStandardButtons(QMessageBox::Close);

    addLabels(vct);

    addGestioneEsito(tamponeorsierologico);

    addConnections();
}

void T_rowVisitaTest::addLabels(const Vector<QString> & vct) {

    if(! vct.empty()){
        id = new QLabel(vct[0],this);
        id->setFixedWidth(20);

        datavisita = new QLabel(vct[1],this);
        datavisita->setFixedWidth(150);

        cf = new QLabel(vct[2],this);
        cf->setFixedWidth(200);

        email = new QLabel(vct[3],this);
        email->setFixedWidth(200);

        recapito = new QLabel(vct[4],this);
        recapito->setFixedWidth(150);

        tipotest = new QLabel(vct[5],this);
        tipotest->setFixedWidth(150);

        QString txtesito = (vct[6].toInt()==-1)?"?":(vct[6].toInt()==0?"N":"P");
        esito = new QLabel(txtesito,this);
        esito->setFixedWidth(20);

        erogata = esito->text().toInt() == 1 || esito->text().toInt() == 0;

        mainLayout->addWidget(id,1,1);
        mainLayout->addWidget(datavisita,1,2);
        mainLayout->addWidget(cf,1,3);
        mainLayout->addWidget(email,1,4);
        mainLayout->addWidget(recapito,1,5);
        mainLayout->addWidget(tipotest,1,6);
        mainLayout->addWidget(esito,1,7);

        id->setObjectName("id");
        datavisita->setObjectName("data");
        cf->setObjectName("cf");
        email->setObjectName("email");
        recapito->setObjectName("recapito");
        tipotest->setObjectName("tipotest");
        esito->setObjectName("esito");

    }else{
        datavisita=cf=email=recapito=tipotest=esito= nullptr;
        mainLayout->addWidget(new QLabel("Nessuna visita per test covid-19 presente nel sistema"));
    }
}

void T_rowVisitaTest::addGestioneEsito(bool tamponeorsierologico) {
}

void T_rowVisitaTest::addConnections() {
}

void T_rowVisitaTest::edit_esito() {
}

void T_rowVisitaTest::send() {
    bool tamponeOrSierologico  = valoresierologico == nullptr;
    int esito=-1;
    int val=0;

    if(tamponeOrSierologico){
        if(rbpositivo->isChecked()){
            esito = 1;
        }
        if(rbnegativo->isChecked()){
            esito = 0;
        }
        if(!rbpositivo->isChecked() && !rbnegativo->isChecked()){
            esito=-1;
        }
    }else{
        val = valoresierologico->text().toDouble();
        esito = val>50?1:0;
    }


    if(true){
        emit send_esito(get_row_id(), tamponeOrSierologico,esito,val);
        gestionee_esito->setEnabled(false);
        edit->setEnabled(false);
    }
}

int T_rowVisitaTest::get_row_id() const {
    return id->text().toInt();
}

int T_rowVisitaTest::get_label_esito() const {
    if(esito){
        if(esito->text()=="?"){
            return -1;
        }else{
            return (esito->text().toInt());
        }
    }else{
        return -1;
    }
}

QString T_rowVisitaTest::get_label_cf() const {
    if(cf){
        return (cf->text());
    }else{
        return "";
    }
}

bool T_rowVisitaTest::dataValidation(bool tamponeORsierologico) {
    bool all_is_validate = false;
    QString value_to_validate;
    QString err;
    int n= 0;
    if(tamponeORsierologico){
        //tampone
        if(rbnegativo->isChecked() || rbpositivo->isChecked()){
            all_is_validate=true;
        }else{
            err+="Seleziona almeno un radio button tra 'positivo' e 'negativo'";
            data_err_popup->setText(err);
            data_err_popup->setMinimumWidth(1000);
            data_err_popup->show();
            //gestione_esito->setStyle()
        }
    }else{
        //sierologico
        value_to_validate  = valoresierologico->text();
        if(valoresierologico->validator()->validate(value_to_validate,n) == QValidator::Acceptable){
            all_is_validate=true;
        }else {
            err.append("Inserisci valore tra 0 e 100 nel campo dati");
            data_err_popup->setText(err);
            data_err_popup->setMinimumWidth(1000);
            data_err_popup->show();

        }
    }
    return all_is_validate;
}

bool T_rowVisitaTest::get_erogazione() const {
    return erogata;
}

void T_rowVisitaTest::set_erogazione(bool b) {
    erogata = b;
}

QPushButton *T_rowVisitaTest::get_submit() const {
    return submit;
}
