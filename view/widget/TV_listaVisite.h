#ifndef COVIDMANAGER_LISTAVISITE_H
#define COVIDMANAGER_LISTAVISITE_H

#include <QWidget>
#include <QScrollArea>
#include <QPushButton>
#include <QVBoxLayout>
#include <model/gerarchia/Visita.h>
#include <model/gerarchia/Vaccinazione.h>
#include "model/Vector.h"
#include "model/gerarchia/Base.h"
#include "model/DeepPtr.h"
#include "view/widget/V_rowVisitaVaccinazione.h"
#include "model/Algo/algorithm.h"
#include <view/widget/T_rowVisitaTest.h>
#include <QUrl>
#include <QUrl>
#include <model/gerarchia/Tampone.h>
#include <model/gerarchia/Sierologico.h>
#include <QJsonArray>


class TV_listaVisite : public QWidget{
Q_OBJECT

public:
    TV_listaVisite(Controller *c = nullptr, bool = true, bool = false, const QString& ="", const QString& ="", const QString& ="", int=0);//erogate, non erogate, cf, nome file, nome array di obj dentro json
    ~TV_listaVisite() = default;
    void updateVisite(bool,bool,QString,int);
    void updateEsito(int,bool,int,double,QString, QString);
    void updateStato(int,bool,OperatoreSanitario*,QString,QString);
    void updateStatoVax(int, bool, OperatoreSanitario*, QString, QString);
    void updatePaziente(int,QString,QString,QString,QString,QString);
    void updatePazienteVax(int, QString, QString, QString, QString, QString, QString, QString, QString, Data, HealthData);
    void add_visita_vax(Vaccinazione);

    Vector<T_rowVisitaTest*>* getTestRows() const;
    Vector<V_rowVisitaVaccinazione*>* getVaxRows() const;
    unsigned int getListaSize() const;

signals:
    /**
     * @brief       segnale inviato al controller per aggiornare l'esito della visita (identificata con id)
     * @param1 id    visita da aggiornare
     * @param2 torv  indica se visita è tampone (== true), o sierologico (== false)
     * @param3 esito indica il nuovo valore dell'esito da aggiornare
     * @param4 val   indica (se esiste) il nuovo valore del test sierologico da aggiornare
     */
    void send_esito(int,bool,int,double);

private:
    QWidget         *container;
    QVBoxLayout     *mainlayout;
    QScrollArea     *scrollarea;
    QUrl *url;

    Vector<DeepPtr<Base>> listaVisite;
    Vector<V_rowVisitaVaccinazione*> *listaRowVax;
    Vector<T_rowVisitaTest*>         *listaRowTest;
    QLabel          *empty;

    /**
     * @brief   costruisce i widget V_rowVisitaVaccinazione da immettere nel vector listaRowVax
     * @param1   erogate parametro che indica se i widget da costruire sono quelli relativi alle visite già erogate (==true) o no (==false)
     * @param2   cf      parametro che indica se i widget da costruire sono quelli relativi alle visite prenotate da un paziente con un determinato codice fiscale
     */
    void buildVaxRows(bool erogate=false,const QString &cf ="");

    /**
    * @brief   costruisce i widget T_rowVisitaTest da immettere nel vector listaRowTest
    * @param1   erogate parametro che indica se i widget da costruire sono quelli relativi alle visite già erogate (==true) o no (==false)
    * @param2   esito   parametro che indica se i widget da costruire sono quelli relativi alle visite con un determinato esito (0 negativo, 1 positivo, -1 in elaborazione)
    * @param3   cf      parametro che indica se i widget da costruire sono quelli relativi alle visite prenotate da un paziente con un determinato codice fiscale
    */
    void buildTestRows(bool erogate=false,int esito=0,const QString& cf ="");

    /**
     * @brief   aggiunge le rows costruite al mainlayout
     */
    void addRows();

    /**
     * @brief   pulisce i vector listaRowVax (se != nullptr) e listaRowTest (se != nullptr) e pulisce il layout mainlayout da tutti i widget in esso presenti
     */
    void clearVisite();

};
#endif //COVIDMANAGER_LISTAVISITE_H
