#ifndef COVIDMANAGER_T_ROWVISITATEST_H
#define COVIDMANAGER_T_ROWVISITATEST_H

#include <QWidget>
#include <QLabel>
#include <QHBoxLayout>
#include <QPushButton>
#include <QRadioButton>
#include <QGroupBox>
#include <QLineEdit>
#include "model/Vector.h"
#include <QDateTime>
#include <QMessageBox>
#include <QDoubleValidator>

class Controller;

class T_rowVisitaTest: public QWidget {
    Q_OBJECT

public:
    T_rowVisitaTest(Controller * = nullptr, QWidget * = nullptr, const Vector<QString>& = Vector<QString>(), bool = true);
    ~T_rowVisitaTest() = default;
    int get_row_id() const;
    int get_label_esito() const;
    QString get_label_cf() const;
    bool get_erogazione() const;
    void set_erogazione(bool);
    QPushButton* get_submit() const;


public slots:
    /**
     * @brief   imposta il widget QGroupBox abilitato (nel caso fosse disabilitato) e disabilitato (nel caso fosse abilitato)
     */
    void edit_esito();

    /**
     * @brief   raccoglie i dati dai widget del componente ed emette (se validazione superata) il segnale send_esito(int,bool,int,double)
     */
    void send();

signals:
    /**
     * @brief       segnale inviato al controller per aggiornare l'esito della visita (identificata con id)
     * @param1 id    visita da aggiornare
     * @param2 torv  indica se visita è tampone (== true), o sierologico (== false)
     * @param3 esito indica il nuovo valore dell'esito da aggiornare
     * @param4 val   indica (se esiste) il nuovo valore del test sierologico da aggiornare
     */
    void send_esito(int id,bool torv, int esito, double val);

private:
    QGridLayout     *mainLayout;
    QHBoxLayout     *gestione_esito_layout;

    QLabel *id;
    QLabel *cf;
    QLabel *email;
    QLabel *recapito;
    QLabel *datavisita;
    QLabel *tipotest;
    QLabel *esito;

    QGroupBox       *gestionee_esito;
    QPushButton     *edit;
    QPushButton     *submit;
    QRadioButton    *rbpositivo;
    QRadioButton    *rbnegativo;
    QLineEdit       *valoresierologico;
    QMessageBox     *data_err_popup;

    bool erogata;

    /**
     * @brief   aggiunge al mainlayout i widget QLabel valorizzandoli con i valori presento nel Vector v
     * @param1  v   vector contenente i valori QString da settare nelle QLabel
     */
    void addLabels(const Vector<QString>& v);

    /**
     * @brief   aggiunge al mainlayout i widget per la gestione (e l'inserimento) dell'esito
     * @brief   indica se i widget da aggiungere sono relativi ad un tampone o ad un sierologico
     */
    void addGestioneEsito(bool tamponeorsierologico = true);

    /**
     *  @brief  connette widget, slot, sengali
     */
    void addConnections();

    /**
     * @brief   aggiunge al mainlayout i widget validatori nei widget QLineEdit e gestisce i popup nel caso di errori
     * @param1   indica se i validatori da aggiungere sono relativi ai widget tampone o sierologico
     */
    bool dataValidation(bool tamponeORsierologico);
};


#endif //COVIDMANAGER_T_ROWVISITATEST_H
