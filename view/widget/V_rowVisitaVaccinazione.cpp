#include "V_rowVisitaVaccinazione.h"

V_rowVisitaVaccinazione::V_rowVisitaVaccinazione(Controller * cont, QWidget * p, Vector<QString> vct) {
    mainLayout = new QHBoxLayout(this);

    addLabels(vct);

}

void V_rowVisitaVaccinazione::addLabels(const Vector<QString>& vct) {
    if(! vct.empty()){

        nome = new QLabel(vct[0],this);
        nome->setFixedWidth(150);
        cognome = new QLabel(vct[1],this);
        cognome->setFixedWidth(150);
        data_nascita = new QLabel(vct[2],this);
        data_nascita->setFixedWidth(150);
        c_f = new QLabel(vct[3],this);
        c_f->setFixedWidth(200);
        tipoVaccino = new QLabel(vct[5],this);
        tipoVaccino->setFixedWidth(150);


        mainLayout->addWidget(nome);
        mainLayout->addWidget(cognome);
        mainLayout->addWidget(data_nascita);
        mainLayout->addWidget(c_f);
        mainLayout->addWidget(tipoVaccino);

    }else{
        nome=cognome=data_nascita=c_f=tipoVaccino= nullptr;
        mainLayout->addWidget(new QLabel("Nessuna visita per vaccinazione presente nel sistema"));
    }
}
