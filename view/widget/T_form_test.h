#ifndef COVIDMANAGER_T_FORM_TEST_H
#define COVIDMANAGER_T_FORM_TEST_H
#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QGridLayout>
#include <QRadioButton>
#include <QPushButton>
#include <QGroupBox>
#include "model/Vector.h"
#include "model/Algo/algorithm.h"
#include <QUrl>
#include <QRegularExpression>
#include <QRegularExpressionValidator>
#include <QMessageBox>
#include <QFileDialog>
#include <QPainter>
#include <QPrinter>
#include <QTextDocument>
#include <QDesktopServices>

class Controller;

class T_form_test: public QWidget {

Q_OBJECT;
public:

    T_form_test(Controller* = nullptr, QWidget * = nullptr, bool = false, unsigned int = 0);
    ~T_form_test() = default;

    void next_visita(int);

public slots:
    /**
     * @brief emette il segnale paziente_successivo() che carica nel form il paziente successivo (se esiste), gestisce di conseguenza i bottoni
     */
    void send_next();

    /**
     * @brief
     */
    void edit_form();

    /**
     * @brief   sblocca l'editing dei campi del form, gestisce di conseguenza i bottoni, emette il segnale aggiorna_paziente per l'update del paziente modificato
     */
    void update_data();

    /**
     * @brief   gestisce i bottoni dopo il click di somminsitrazione
     */
    void manage_ok_button();

    /**
     * @brief   gestisce il bottone stampa (cliccabile solo una volta)
     */
    void manage_stampa();

signals:
    /**
     *  @brief  segnale inviato al controller per caricare nel form (se esiste) il paziente successivo
     */
    void paziente_successivo();

    /**
     * @brief   segnale inviato al controller per aggiornare i campi dati del paziente  paziente
     * @param1  codice fiscale
     * @param2  email
     * @param3  numero di telefono
     */
    void aggiorna_paziente(QString,QString,QString);

private:
    Controller  *controller;
    QLabel      *cf, *recapito, *email, *finish;
    QLineEdit   *cfEdit, *emailEdit, *telEdit;
    QVBoxLayout *mainlayout;
    QGridLayout *layoutData;
    QHBoxLayout *layoutButtons, *layoutFinish;
    QRadioButton *antigenico, *molecolare, *sierologico;
    QGroupBox    *groupData, * groupButtons, *groupFinish;
    QPushButton  *ok, *somministra, *stampa,*edit, *update;
    QMessageBox  *data_err_popup;

    /**
     * @brief   aggiunge al form widget QLabel e QLineEdit, sse isDataLoaded=true li imposterà come text nei widget QLineEdit
     * @param   isDataLoaded        indica se i dati sono precaricati o meno
     * @param   codicefiscale       indica l'eventuale QString da immettere nel widget QLineEdit cfEdit
     * @param   recapitoemail       indica l'eventuale QString da immettere nel widget QLineEdit emeilEdit
     * @param   recapitoloTelefono  indica l'eventuale QString da immettere nel widget QLineEdit telEdit
     */
    void addLabelsAndLineEdits(bool isDataLoaded=false, QString codicefiscale="", QString recapitoemail="", QString recapitoloTelefono="");

    /**
     * aggiunge al mainlayout i radiobutton che indicano il tipo di test (antigenico, molecolare, sierologico)
     */
    void addRadioButtons(bool = false, int = 0);

    /**
     *  aggiunge al mainlayout i bottoni di gestione (ok, somministra, stampa, edit, update)
     */
    void addActionsButtons();

    /**
     *  @brief  connette widget, slot, sengali
     */
    void addConnections();

    /**
     * @brief   aggiunge i validatori nei widget QLineEdit e gestisce i popup nel caso di errori
     */
    bool dataValidation();

    void addFinishLabel();
};

#endif //COVIDMANAGER_T_FORM_TEST_H
