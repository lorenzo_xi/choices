#include "TV_barraRicerca.h"

TV_barraRicerca::TV_barraRicerca(Controller *c, QWidget *parent, bool vot) : QWidget(parent), controller(c)    {

    mainLayout = new QHBoxLayout(this);
    gridLayout = new QGridLayout();

    vaxortest=vot;

    addSearchElements();

    addConnections();

    // Aggiungere i layout
    mainLayout->addLayout(gridLayout);
    setLayout(mainLayout);

}

void TV_barraRicerca::addSearchElements() {
    b_erogate = new QRadioButton("Erogate");
    b_erogate->setChecked(false);
    b_nonerogate = new QRadioButton("Non erogate");
    b_nonerogate->setChecked(true);

    groupBasicSearch = new QGroupBox("Ricerca:");
    hbox1 = new QHBoxLayout;
    hbox1->addWidget(b_erogate);
    hbox1->addWidget(b_nonerogate);

    inputfield = new QLineEdit();
    inputfield->setText("");
    inputfield->setEnabled(false);
    inputfield->setPlaceholderText("Inserisci il CF che vuoi cercare");
    inputfield->setMaxLength(16);

    insertcf = new QCheckBox("Cerca per CF");

    hbox1->addWidget(insertcf);
    hbox1->addWidget(inputfield);

    groupBasicSearch->setLayout(hbox1);

    gridLayout->addWidget(groupBasicSearch);

    search = new QPushButton("CERCA");
    gridLayout->addWidget(search);
}


void TV_barraRicerca::addConnections() {

    connect(insertcf,SIGNAL(clicked()), this, SLOT(manage_cf()));
    connect(search,SIGNAL(clicked()), this, SLOT(send()));

}

void TV_barraRicerca::manage_cf() {
    if(insertcf->isChecked()){
        inputfield->setEnabled(true);
    }else{
        inputfield->setEnabled(false);
        inputfield->clear();
    }
}

void TV_barraRicerca::send(){
    bool flagVaxOrTest  = vaxortest;
    bool flagErogate    = b_erogate->isChecked();
    int  flagEisto      =    -1;

    QString flagCF = inputfield->text();

    emit send_query(flagVaxOrTest,flagErogate,flagCF,flagEisto);
}
