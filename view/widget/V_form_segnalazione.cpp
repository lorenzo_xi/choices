#include "V_form_segnalazione.h"

V_form_segnalazione::V_form_segnalazione(Controller *controller, QWidget *parent){
    mainLayout = new QVBoxLayout(this);

    addPopups();

    textarea = new QTextEdit();
    textarea->setPlaceholderText("Scrivi qui la segnalazione...");

    submit = new QPushButton("INVIA SEGNALAZIONE");

    mainLayout->addWidget(textarea);
    mainLayout->addWidget(submit);

    addConnections();
}

void V_form_segnalazione::check() {

    if(textarea->toPlainText().isEmpty()){
        popup_errore->show();

    }else{
        popup_conferma->show();

    }
}

void V_form_segnalazione::addPopups() {
    popup_conferma = new QMessageBox(this);
    popup_conferma->setText("Conferma invio segnalazione");
    popup_conferma->setInformativeText("Conferma l'invio della segnalazione, se non sei sicuro di quanto scritto, ricontrolla");
    popup_conferma->setIcon(QMessageBox::Question);
    popup_conferma->setFixedSize(1000,1000);
    yes = popup_conferma->addButton(QMessageBox::Ok);
    no = popup_conferma->addButton(QMessageBox::Cancel);

    popup_errore = new QMessageBox(this);
    popup_errore->setText("Errore");
    popup_errore->setInformativeText("Non è possibile inviare una segnalazione senza testo, scrivi qualcosa");
    popup_errore->setIcon(QMessageBox::Warning);
}

void V_form_segnalazione::addConnections() {
    connect(submit,SIGNAL(clicked()),this,SLOT(check()));
    connect(yes,SIGNAL(clicked()),this,SLOT(send_data()));
}

void V_form_segnalazione::send_data() {
    QJsonObject obj;
    QDateTime tmp = QDateTime::currentDateTime();
    Data d(tmp.date().day(), tmp.date().month(),tmp.date().year());
    Ora o(tmp.time().hour(), tmp.time().minute(),tmp.time().second());
    DataOra d_o(d,o);
    obj["time"] = QString::fromStdString(d_o.stringify());
    obj["text"] = textarea->toPlainText();
    QJsonArray vobj;
    vobj.append(obj);
    Algorithm::write("resources/jsondata/segnalazioni.json","segnalazioni",vobj);
    textarea->clear();
}
