
#ifndef COVIDMANAGER_V_FORM_VACCINI_H
#define COVIDMANAGER_V_FORM_VACCINI_H

#include <QWidget>
#include <QLabel>
#include <QLineEdit>
#include <QGridLayout>
#include <QRadioButton>
#include <QPushButton>
#include <QGroupBox>
#include <QDateEdit>
#include <QDate>
#include <QComboBox>
#include <model/gerarchia/Vaccinazione.h>
#include "model/Vector.h"
#include "model/Algo/algorithm.h"
#include <QMessageBox>
#include <model/Types/Data.h>
#include <QSpinBox>
#include <QUrl>
#include <QFileDialog>
#include <QPrinter>
#include <QTextDocument>
#include <QDesktopServices>
#include <QStringList>
#include <QList>
#include <model/gerarchia/Paziente.h>
#include <model/gerarchia/Vaccinazione.h>
#include <model/Algo/algorithm.h>

class Controller;

class V_form_vaccini: public QWidget {
    Q_OBJECT;
public:
    V_form_vaccini(Controller* = nullptr, QWidget * = nullptr, bool = false, bool = false, unsigned int = 0);
    ~V_form_vaccini() = default;

    void next_visita(int);
    void disableEditButton();
public slots:
    void send_next();
    void edit_form();
    void update_data();
    void manage_ok_button();
    void manage_stampa();
    void salva_visita_extra();
    void clear_form();
    void calcoloRischio();

signals:
    void paziente_successivo();
    void aggiorna_paziente(QString,QString,QString, QString, QString, QString, Data, HealthData);
    void send_visita_extra(Vaccinazione);

private:
    Controller *controller;
    QLabel *nome, *cognome, *CF, *email, *telefono, *hadCovid, *dataNascita, *patologie, *rischioCalc, *rischioEdit,*finish;
    QLineEdit *nomeEdit, *cognomeEdit, *CFEdit, *emailEdit, *telEdit, *dataNEdit;
    QSpinBox *patologieEdit;
    QVBoxLayout *mainlayout;
    QGridLayout *layoutData, *layoutCartella;
    QHBoxLayout *layoutButtons, *layoutComboBox, *layoutFinish;
    QRadioButton *maschio, *femmina, *nobinary, *covidYes, *covidNo;
    QGroupBox    *groupData, * groupButtons, *groupComboBox, *groupCartella, *groupFinish;
    QPushButton  *ok, *somministra, *stampa,*edit, *update, *clear, *calcolaRischio;

    QComboBox *menuVax;
    QMessageBox *data_err_popup, *rischio_err_popup;
    QDateEdit *qDateEdit;
    Data d;

    void addLabelsAndLineEdits(bool = false, QString="", QString="",  QString ="", QString ="", QString ="", QString = "");
    void addRadioButtonsSesso(bool = false, QString="");
    void addComboBoxVax(bool = false , QString="");
    void addCartellaClinica(bool = false, QString="", int=0);
    void addActionsButtons();
    void addConnections(bool=false);
    bool dataValidation();
    void addFinishLabel();
};

#endif //COVIDMANAGER_V_FORM_VACCINI_H
