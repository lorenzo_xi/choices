#include "TV_listaVisite.h"

TV_listaVisite::TV_listaVisite(Controller * c,bool vaxortest, bool flagErogate, const QString & cf, const QString& filename, const QString& arrayname, int esito){

    QUrl path("resources/jsondata/" + filename);
    Algorithm::readFile_toVectorDeepPtr(listaVisite, path.path().toStdString(),arrayname.toStdString());

    if(vaxortest){
        listaRowTest= nullptr;
        listaRowVax = new Vector<V_rowVisitaVaccinazione*>;
        buildVaxRows(flagErogate,cf);
    }else{
        listaRowVax= nullptr;
        listaRowTest = new Vector<T_rowVisitaTest*>;
        buildTestRows(flagErogate,esito,cf);
    }

    container   = new QWidget(this);

    mainlayout  = new QVBoxLayout(this);
    scrollarea = new QScrollArea(this);


    scrollarea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    scrollarea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    scrollarea->setContentsMargins(100,0,0,0);
    scrollarea->setWidgetResizable(true);
    scrollarea->setAlignment(Qt::AlignLeft);
    scrollarea->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);

   scrollarea->setFixedWidth(1200);
   scrollarea->setFixedHeight(370);

    container->setObjectName("container");
    mainlayout->setAlignment(Qt::AlignTop);


    addRows();
    container->setLayout(mainlayout);
    scrollarea->setWidget(container);
}

unsigned int TV_listaVisite::getListaSize() const {
    return listaVisite.getSize();
}

void TV_listaVisite::buildVaxRows(bool erogate, const QString& cf){
    for(auto & it : listaVisite){
        Vector<QString> tmp;
        Vaccinazione* vtmp = dynamic_cast<Vaccinazione *>(it.get());

        if(vtmp){
            tmp.push_back(QString::fromStdString(vtmp->getPaziente()->getNome()));
            tmp.push_back(QString::fromStdString(vtmp->getPaziente()->getCognome()));
            tmp.push_back(QString::fromStdString(vtmp->getPaziente()->getData_Nascita().stringify()));
            tmp.push_back(QString::fromStdString(vtmp->getPaziente()->getCF()));
            tmp.push_back(vtmp->isRichiamo() ? "si":"no");
            tmp.push_back(QString::fromStdString(Algorithm::fromEnumVaxToStdString(vtmp->get_azienda_produttrice())));
            tmp.push_back(vtmp->isDone() ? "si":"no");

            if(erogate){
                if(cf.isEmpty() && vtmp->isDone()){
                    V_rowVisitaVaccinazione *tmp_row = new V_rowVisitaVaccinazione(nullptr, this, tmp);
                    listaRowVax->push_back(tmp_row);
                }else if(!cf.isEmpty() && vtmp->isDone() && vtmp->getPaziente()->getCF()==cf.toStdString()){
                    V_rowVisitaVaccinazione *tmp_row = new V_rowVisitaVaccinazione(nullptr, this, tmp);
                    listaRowVax->push_back(tmp_row);
                }


            }else{
                if(cf.isEmpty() && !vtmp->isDone()){
                    V_rowVisitaVaccinazione *tmp_row = new V_rowVisitaVaccinazione(nullptr, this, tmp);
                    listaRowVax->push_back(tmp_row);
                }else if(!cf.isEmpty() && !vtmp->isDone() && vtmp->getPaziente()->getCF()==cf.toStdString()){
                    V_rowVisitaVaccinazione *tmp_row = new V_rowVisitaVaccinazione(nullptr, this, tmp);
                    listaRowVax->push_back(tmp_row);
                }
            }
        }
        tmp.clear();
    }
}

void TV_listaVisite::buildTestRows(bool erogate,int esito, const QString& cf){
    for(auto & it : listaVisite){

        Vector<QString> tmp;

        Sierologico* s_tmp = dynamic_cast<Sierologico *>(it.get());
        Tampone*     t_tmp = dynamic_cast<Tampone *>(it.get());

        if(s_tmp){

            tmp.push_back( QString::number(s_tmp->getID_Evento()));
            tmp.push_back(QString::fromStdString(s_tmp->getPaziente()->getData_Nascita().stringify()));
            tmp.push_back(QString::fromStdString(s_tmp->getPaziente()->getCF()));
            tmp.push_back(QString::fromStdString(s_tmp->getPaziente()->getEmail()));
            tmp.push_back(QString::fromStdString(s_tmp->getPaziente()->getTelefono()));
            tmp.push_back("Sierologico");
            tmp.push_back(QString::number(s_tmp->get_esito()));


            if(erogate){

                if(cf.isEmpty() && s_tmp->isDone()){
                    T_rowVisitaTest *tmp_row = new T_rowVisitaTest(nullptr, this, tmp, false);
                    listaRowTest->push_back(tmp_row);
                }else if(!cf.isEmpty() && s_tmp->isDone() && s_tmp->getPaziente()->getCF()==cf.toStdString()){
                    T_rowVisitaTest *tmp_row = new T_rowVisitaTest(nullptr, this, tmp, false);
                    listaRowTest->push_back(tmp_row);
                }


            }else{
                if(cf.isEmpty() && !s_tmp->isDone()){
                    T_rowVisitaTest *tmp_row = new T_rowVisitaTest(nullptr, this, tmp, false);
                    listaRowTest->push_back(tmp_row);
                }else if(!cf.isEmpty() && !s_tmp->isDone() && s_tmp->getPaziente()->getCF()==cf.toStdString()){
                    T_rowVisitaTest *tmp_row = new T_rowVisitaTest(nullptr, this, tmp, false);
                    listaRowTest->push_back(tmp_row);
                }

            }

        }else if(t_tmp){

            tmp.push_back(QString::number(t_tmp->getID_Evento()));
            tmp.push_back(QString::fromStdString(t_tmp->getPaziente()->getData_Nascita().stringify()));
            tmp.push_back(QString::fromStdString(t_tmp->getPaziente()->getCF()));
            tmp.push_back(QString::fromStdString(t_tmp->getPaziente()->getEmail()));
            tmp.push_back(QString::fromStdString(t_tmp->getPaziente()->getTelefono()));
            tmp.push_back(QString::fromStdString(Algorithm::fromEnumTampToStdString(t_tmp->getTipoTampone())));
            tmp.push_back(QString::number(t_tmp->get_esito()));

            if(erogate){

                if(cf.isEmpty() && t_tmp->isDone()){
                    T_rowVisitaTest *tmp_row = new T_rowVisitaTest(nullptr, this, tmp, false);
                    listaRowTest->push_back(tmp_row);
                }else if(!cf.isEmpty() && t_tmp->isDone() && t_tmp->getPaziente()->getCF()==cf.toStdString()){
                    T_rowVisitaTest *tmp_row = new T_rowVisitaTest(nullptr, this, tmp, false);
                    listaRowTest->push_back(tmp_row);
                }


            }else{
                if(cf.isEmpty() && !t_tmp->isDone()){
                    T_rowVisitaTest *tmp_row = new T_rowVisitaTest(nullptr, this, tmp, false);
                    listaRowTest->push_back(tmp_row);
                }else if(!cf.isEmpty() && !t_tmp->isDone() && t_tmp->getPaziente()->getCF()==cf.toStdString()){
                    T_rowVisitaTest *tmp_row = new T_rowVisitaTest(nullptr, this, tmp, false);
                    listaRowTest->push_back(tmp_row);
                }

            }


        }
        tmp.clear();
    }
}

void TV_listaVisite::addRows() {
    if(listaRowVax != nullptr){
        empty = nullptr;
        for(auto & it : *listaRowVax){
            mainlayout->addWidget(it);
        }
    }else if(listaRowTest != nullptr){
        empty = nullptr;
        for(auto & it : *listaRowTest){
            if(it->get_label_esito() != -1){
                it->setEnabled(false);
            }

            mainlayout->addWidget(it);
        }
    }else{
        empty = new QLabel("File vuoto!");
        mainlayout->addWidget(empty);
    }
}

void TV_listaVisite::clearVisite() {

    if(listaRowVax){
        listaRowVax->clear();
    }
    if(listaRowTest){
        listaRowTest->clear();
    }

    if ( mainlayout != nullptr ){
        QLayoutItem* item;
        while ( ( item = mainlayout->layout()->takeAt( 0 ) ) != nullptr )
        {
            delete item->widget();
            delete item;
        }
        delete mainlayout->layout();
    }

    mainlayout  = new QVBoxLayout(this);
    mainlayout->setAlignment(Qt::AlignTop);
}

void TV_listaVisite::updateVisite(bool vaxortest, bool erogate, QString cf, int esito) {

    clearVisite();

    if(vaxortest){  //vax
        listaRowTest = nullptr;
        listaRowVax = new Vector<V_rowVisitaVaccinazione*>;
        buildVaxRows(erogate,cf);

    }else{
        listaRowVax = nullptr;
        listaRowTest = new Vector<T_rowVisitaTest*>;
        buildTestRows(erogate,esito,cf);

    }

    addRows();

    container->setLayout(mainlayout);
    scrollarea->setWidget(container);
}

void TV_listaVisite::updateEsito(int id, bool tampORsiero, int esito, double val_siero, QString filename, QString arrayname) {

    bool trovato=false;
    if(tampORsiero){
        for(auto it=listaVisite.begin(); it!=listaVisite.end() && !trovato; ++it){
            Tampone* tmp = dynamic_cast<Tampone*>(it->get());
            if(tmp && tmp->getID_Evento() == id){
                tmp->set_esito(esito);
                tmp->set_stato(true);
                trovato=true;
            }
        }
    }else{
        for(auto it=listaVisite.begin(); it!=listaVisite.end() && !trovato; ++it){
            Sierologico* tmp = dynamic_cast<Sierologico*>(it->get());
            if(tmp && tmp->getID_Evento() == id){
                tmp->set_quantita(val_siero);
                tmp->set_esito(val_siero>50?1:0);
                tmp->set_stato(true);
                trovato=true;
            }
        }
    }

    QUrl path("resources/jsondata/"+filename);
    Algorithm::update(listaVisite, path.path().toStdString(),arrayname.toStdString());

}

void TV_listaVisite::updateStato(int id, bool value, OperatoreSanitario* op,QString filename, QString arrayname) {
    bool trovato=false;
    for(auto it=listaVisite.begin(); it!=listaVisite.end() && !trovato; ++it){
        Tampone* tmp_t = dynamic_cast<Tampone*>(it->get());
        Sierologico* tmp_s = dynamic_cast<Sierologico*>(it->get());

        if(tmp_t && tmp_t->getID_Evento() == id){
            tmp_t->set_stato(value);
            tmp_t->set_prezzo(tmp_t->prezzoTotale());
            tmp_t->setDurataTotale(tmp_t->durata());
            trovato=true;
        }else if(tmp_s){
            if(tmp_s && tmp_s->getID_Evento() == id){
                tmp_s->set_riferimento(*op);
                tmp_s->set_stato(value);
                tmp_s->set_prezzo(tmp_s->prezzoTotale());
                tmp_s->setDurataTotale(tmp_s->durata());
                trovato=true;
            }
        }
    }

    QUrl path("resources/jsondata/"+filename);
    Algorithm::update(listaVisite, path.path().toStdString(),arrayname.toStdString());
}

void TV_listaVisite::updateStatoVax(int id, bool value, OperatoreSanitario* op, QString filename, QString arrayname) {
    bool trovato=false;
    for(auto it=listaVisite.begin(); it!=listaVisite.end() && !trovato; ++it){
        Vaccinazione* vax = dynamic_cast<Vaccinazione*>(it->get());

        if(vax && vax->getID_Evento() == id){
            vax->set_riferimento(*op);
            vax->set_stato(value);
            vax->setDurataTotale(vax->durata());
            vax->set_prezzo(vax->prezzoTotale());
            trovato=true;
        }
    }
    QUrl path("resources/jsondata/"+filename);
    Algorithm::update(listaVisite, path.path().toStdString(),arrayname.toStdString());
}

void TV_listaVisite::updatePaziente(int id,QString cf, QString email, QString recapito, QString filename, QString arrayname) {
    bool trovato=false;

    for(auto it=listaVisite.begin(); it!=listaVisite.end() && !trovato; ++it){
        Tampone* tmp_t = dynamic_cast<Tampone*>(it->get());
        Sierologico* tmp_s = dynamic_cast<Sierologico*>(it->get());

        if(tmp_t && tmp_t->getID_Evento() == id){
            tmp_t->getPaziente()->setCF(cf.toStdString());
            tmp_t->getPaziente()->setEmail(email.toStdString());
            tmp_t->getPaziente()->setTelefono(recapito.toStdString());
            trovato=true;
        }else{
            if(tmp_s && tmp_s->getID_Evento() == id){

                tmp_s->getPaziente()->setCF(cf.toStdString());
                tmp_s->getPaziente()->setEmail(email.toStdString());
                tmp_s->getPaziente()->setTelefono(recapito.toStdString());
                trovato=true;
            }
        }
    }

    QUrl path("resources/jsondata/"+filename);
    Algorithm::update(listaVisite, path.path().toStdString(),arrayname.toStdString());
}

void TV_listaVisite::updatePazienteVax(int id,QString nome, QString cognome, QString cf, QString email, QString recapito, QString filename, QString arrayname, QString sesso, Data dataN, HealthData hd) {

    bool trovato=false;

    for(auto it=listaVisite.begin(); it!=listaVisite.end() && !trovato; ++it){
        Vaccinazione* vax = dynamic_cast<Vaccinazione*>(it->get());

        if(vax && vax->getID_Evento() == id){
            vax->getPaziente()->setNome(nome.toStdString());
            vax->getPaziente()->setCognome(cognome.toStdString());
            vax->getPaziente()->setCF(cf.toStdString());
            vax->getPaziente()->setEmail(email.toStdString());
            vax->getPaziente()->setTelefono(recapito.toStdString());
            vax->getPaziente()->setDataNascita(dataN);
            vax->getPaziente()->setCartellaClinica(hd);
            vax->getPaziente()->setSesso(sesso.toStdString());
            trovato=true;
        }
    }
    QUrl path("resources/jsondata/"+filename);
    Algorithm::update(listaVisite, path.path().toStdString(),arrayname.toStdString());
}


Vector<T_rowVisitaTest *> *TV_listaVisite::getTestRows() const {
    return listaRowTest;
}

Vector<V_rowVisitaVaccinazione *>* TV_listaVisite::getVaxRows() const {
    return listaRowVax;
}

void TV_listaVisite::add_visita_vax(Vaccinazione v) {
    Vaccinazione* new_vax = new Vaccinazione(v);
    DeepPtr<Base> dpt(new_vax);
    listaVisite.push_back(dpt);
    QUrl path("resources/jsondata/visite_vax.json");
    Algorithm::update(listaVisite, path.path().toStdString(),"visite_vax");
}
