#ifndef COVIDMANAGER_V_FORM_SEGNALAZIONE_H
#define COVIDMANAGER_V_FORM_SEGNALAZIONE_H

#include <QWidget>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QRadioButton>
#include <QPushButton>
#include <QGroupBox>
#include <QTextEdit>
#include <QDateTime>
#include <model/Algo/algorithm.h>
#include <QMessageBox>
#include <QJsonObject>
#include <QJsonArray>
#include <QDialogButtonBox>

class Controller;

class V_form_segnalazione: public QWidget {
    Q_OBJECT

public:
    V_form_segnalazione(Controller* controller= nullptr, QWidget *parent = nullptr);
    ~V_form_segnalazione() = default;

public slots:
    /**
     * @brief   scrive nel file "segnalazioni.json" un oggetto json con campi "time" e "text valorizzati rispettivamente come il timestamp (orario e fata) attuale e il testo della textarea
     */
    void send_data();

    /**
     * @brief   controlla se textarea è vuota, se lo è apre un popup di errore, altrimenti apre un popup di conferma
     */
    void check();

private:
    Controller      *controller;

    QVBoxLayout     *mainLayout;

    QTextEdit       *textarea;

    QPushButton     *submit, *yes, *no;

    QMessageBox     *popup_conferma, *popup_errore;


    /**
     * @brief   aggiunge i popup
     */
    void addPopups();

    /**
     *  @brief  connette widget, slot, sengali
    */
    void addConnections();
};


#endif //COVIDMANAGER_V_FORM_SEGNALAZIONE_H
