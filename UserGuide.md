---

title:  CovidManager - Progetto di Programmazione ad Oggetti (CdL Informatica UniPD) a.a. 2020/2021.

author: Silvia Giro, Perinello Lorenzo

date:   Giugno 2020

comment: manuale utente

---

# CovidManager - Gestionale per l'erogazione di vaccini e test per SARS-CoV-2

<div align="center"> ![logo](https://gitlab.com/lorenzo_xi/choices/-/raw/d608a054676400d0c30f22ae1691161ebc6b77ae/resources/img/logoCovid.png) </div>

>   il bordo nero (top-left) serve per dare profondità all'immagine e per dare un senso di libertà (o liberazione) dato dalla non presenza dei bordi su tutta l'immagine  : non è un errore.
>   - lo staff di CovidManager


## Autenticazione

1. Cliccare sul pulsante "**Entra**"
2. Inserire il proprio codice fiscale (indicati in relazione o nel file opsanitari.json)
3. Per entrare nella parte dedicata ai vaccini premere sul pulsante "**Vaccina**", per accedere sulla parte dedicata ai test cliccare su "**Testa**".

NB: la sezione 'Vaccina' è disponibile solo per i medici e non per gli infermieri.


## Gestionale Vaccini

In alto a destra è possibile trovare le proprie credenziali di accesso, sotto di esse è presente un menù con varie opzioni.
Sempre in altro a destra, sopra le prorie credeziali di accesso, è presente un bottone "TORNA INDIETRO" per tornare alla scelta iniziale del gestionale (Vaccina/Testa) (Attenzione: il bottone "TORNA INDIETRO" ha solo questa funzionalità e NON serve per spostarsi o tornare indietro tra le opzioni del menù).

Di seguito sono descritte le opzioni del menù:

### Vaccina

Qui è possibile erogare la vaccinazione in base alla lista di prenotazione sequenziale (file visite_vax.json).

0. Se necessario, è possibile modificare i dati del paziente cliccando su "**EDIT**".
1. Una volta completata l'inoculazione del vaccino, cliccare su "**VISITA EFFETTUATA**"
2. È possibile salvare la ricevuta della visita cliccando "**STAMPA RICEVUTA**": dapprima si deve salvare nel folder indicato (**NON** si deve cambiare path di salvataggio e nome del file), poi una volta salvato si aprirà in automatico la ricevuta che può essere stampata come un classico file in base al sistema operativo e al visualizzatore di pdf in uso. 
3. Dopo il click, si sbloccherà il pulsante "**PROSSIMO PAZIENTE**", cliccandolo si procederà con la prossima visita.

NB: Nelle inoculazioni prenotate erogate in questa sezione non si possono modificare i campi dati relativi al vaccino (una volta prenotata la visita per la vaccinazione non si può cambiare la tipologia di vaccino all'ultimo per questioni logistico-organizzative).

### Lista Pazienti

Qui è possibile visualizzare le inoculazioni già effettuate o ancora da fare.
Premere su "**CERCA**" per aggiornare la lista e visualizzare le visite corrispondenti (Attenzione: la lista NON si aggiorna da sola, bisogna premere "**CERCA**" per aggiornarla).
I filtri di ricerca sono "**EROGATE**" (visite già fatte), "**NON EROGATE**" (visite da fare), e "CF" (un parametro di ricerca per codice fiscale).
Per cercare tramite codice fiscale è necessario sbloccare il campo cliccando sul checkbox corrispondente e poi scriverlo nell'apposito campo di input presente a destra del checkbox.


### Paziente Extra

Qui è possibile inserire i dati relativi ad un'inoculazione extra che non è presente nella lista di visite prenotate.
Il procedimento dell'erogazione di questo paziente extra è lo stesso di "Vaccina", eccetto il fatto che dopo il click di "**EDIT**" cliccando su "**CALCOLA RISCHIO**" è possibile (ma non strettamente necessario) calcolare il rischio del Paziente (in base ai suoi dati), una volta calcolato questo parametro verà visualizzato sul form.
In questo caso il vaccino potrà essere scelto al momento della visita extra (in base alle disponibilità ed alle esigenze logistico-organizzative).

NB: Nelle inoculazioni extra una volta selezionato il vaccino il campo non si può più modificare (), è bene dunque stare attenti e operare la giusta scelta.


### Segnalazione

Qui è possibile scrivere una segnalazione (evento avverso, reazione allergica, svenimento...) ed inviarla. Le segnalazioni vengono salvate su un file (segnalazioni.json) per essere conservate.
Non è possibile rivederle o modificarle una volta inviate.
Per inviare la segnalazione, è necessario cliccare su "**INVIA SEGNALAZIONE**".
 

### Esci
Esce dal programma.



## Gestionale Tamponi

In alto a destra è possibile trovare le proprie credenziali di accesso, sotto di esse è presente un menù con varie opzioni.
Sempre in altro a destra, sopra le prorie credeziali di accesso, è presente un bottone "TORNA INDIETRO" per tornare alla scelta iniziale del gestionale (Vaccinazioni/Test) (Attenzione: il bottone "TORNA INDIETRO" ha solo questa funzionalità e NON serve per spostarsi o tornare indietro tra le opzioni del menù).

Di seguito sono descritte le opzioni del menù:

### Testa

Qui è possibile erogare il test in base alla lista di prenotazioni sequenziali (file visite_test.json).

0. Se necessario è possibile modificare i dati del paziente cliccando su "**EDIT**".
1. Una volta effettuato il test o vaccino clicca su "**VISITA EFFETTUATA**"
2. È possibile salvare la ricevuta della visita cliccando "STAMPA RICEVUTA": dapprima si deve salvare nel folder indicato (**NON** si deve cambiare path di salvataggio e nome del file), poi una volta salvato si aprirà in automatico la ricevuta che può essere stampata come un classico file in base al sistema operativo e al visualizzatore di pdf in uso.
3. Dopo il click, si sbloccherà il pulsante "**PROSSIMO PAZIENTE**" cliccandolo si procederà con la prossima visita.

NB: Nei test prenotati (ed erogati) in questa sezione non si possono modificare i campi dati relativi alla tipologia di test (una volta prenotata la visita per il test esso non può essere modificato all'ultimo per questioni logistico-organizzative).

### Lista Pazienti

Qui è possibile visualizzare le inoculazioni già effettuate o ancora da fare.
Premere su "**CERCA**" per aggiornare la lista e visualizzare le visite corrispondenti (Attenzione: la lista NON si aggiorna da sola, bisogna premere "**CERCA**" per aggiornarla).
I filtri di ricerca sono "**EROGATE**" (visite già fatte), "**NON EROGATE**" (visite da fare), e "CF" (un parametro di ricerca per codice fiscale).
Per cercare tramite codice fiscale è necessario sbloccare il campo cliccando sul checkbox corrispondente e poi scriverlo nell'apposito campo di input presente a destra del checkbox.


## Input Dati (pattern dei dati da immettere)

I dati da immettere nel gestionale devono rispettareun pattern ben preciso: i campi di input non accetteranno stringhe di diverso tipo! Nel caso vengano scritti dei dati in maniera scorretta l'evento avverso sarà segnalato all'utente da un apposito pop-up.

Di seguito i pattern da rispettare:

- Nome/Cognome : solo caratteri minuscoli e maiuscoli no numeri o altri caratteri speciali (esempio: Luca Verdi).
- Codice Fiscale: classico pattern del codice fiscale italiano (esempio: ABCDEF01G23H456I).
- Email: classico pattern delle email (esempio: email@servizioemail.it, oppure: email.email@email.email.it).
- Recapito: (3)cifre-(3,4)cifre-(4,8)cifre (esempio: 340-111-2222, NB: (n1,n2) indica un intervallo chiuso tra le cifre n1 ed n2 comprese).
- Patologie: numero intero da 0 a 25

Di seguito alcuni esempi relativi ai possibili errori e ai popup che li segnalano:

### Popup di errore
![Errore popup](https://gitlab.com/lorenzo_xi/choices/-/raw/d608a054676400d0c30f22ae1691161ebc6b77ae/UserGuidePic/errore_email_tmp.png)

### Esempio pattern sbagliato
![Pattern sbagliato](https://gitlab.com/lorenzo_xi/choices/-/raw/d608a054676400d0c30f22ae1691161ebc6b77ae/UserGuidePic/formato_sbagliato_tmp.png)


### Esempio pattern corretti
![Pattern giusto](https://gitlab.com/lorenzo_xi/choices/-/raw/d608a054676400d0c30f22ae1691161ebc6b77ae/UserGuidePic/giusto_formato_vax.png)
