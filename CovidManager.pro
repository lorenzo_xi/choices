QT += core gui
QT += printsupport

greaterThan(QT_MAJOR_VERSION, 4) {
    QT += printsupport
    QT += widgets
}

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

QMAKE_CXXFLAGS_WARN_OFF

SOURCES += \
    model/Algo/algorithm.cpp \
    model/gerarchia/Evento.cpp \
    model/gerarchia/OperatoreSanitario.cpp \
    model/gerarchia/Paziente.cpp \
    model/gerarchia/Sierologico.cpp \
    model/gerarchia/Tampone.cpp \
    model/gerarchia/Test.cpp \
    model/gerarchia/Utente.cpp \
    model/gerarchia/Vaccinazione.cpp \
    model/gerarchia/Visita.cpp \
    model/Types/Data.cpp \
    model/Types/Ora.cpp \
    model/Types/DataOra.cpp \
    model/Types/HealthData.cpp \
    view/manager.cpp \
    view/widget/TV_barraRicerca.cpp \
    view/widget/TV_listaVisite.cpp \
    view/widget/V_rowVisitaVaccinazione.cpp \
    view/gestionale_vax.cpp \
    view/gestionale_tamponi.cpp \
    view/widget/V_form_segnalazione.cpp \
    view/scelta_gestionale.cpp \
    view/widget/T_form_test.cpp \
    view/widget/T_rowVisitaTest.cpp \
    view/widget/V_form_vaccini.cpp \
    view/mainwindow.cpp \
    controller/Controller.cpp \
    main.cpp

HEADERS += \
        model/Algo/algorithm.h \
        model/gerarchia/Base.h \
        model/gerarchia/Evento.h \
        model/gerarchia/OperatoreSanitario.h \
        model/gerarchia/Paziente.h \
        model/gerarchia/Sierologico.h \
        model/gerarchia/Tampone.h \
        model/gerarchia/Test.h \
        model/gerarchia/Utente.h \
        model/gerarchia/Vaccinazione.h \
        model/gerarchia/Visita.h \
        model/Types/Data.h \
        model/Types/Ora.h \
        model/Types/DataOra.h \
        model/Types/HealthData.h \
        model/Types/EnumTypes.h \
        model/DeepPtr.h \
        model/Vector.h \
        view/manager.h \
        view/widget/TV_barraRicerca.h \
        view/widget/TV_listaVisite.h \
        view/widget/V_rowVisitaVaccinazione.h \
        view/gestionale_vax.h \
        view/gestionale_tamponi.h \
        view/widget/V_form_segnalazione.h \
        view/scelta_gestionale.h \
        view/widget/T_form_test.h \
        view/widget/T_rowVisitaTest.h \
        view/widget/V_form_vaccini.h \
        view/mainwindow.h \
        controller/Controller.h
FORMS += \
	mainwindow.ui
# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target


RESOURCES += \
    resources.qrc

DISTFILES += \
    resources/jsondata/segnalazioni.json \
    resources/jsondata/visite_vax.json \
    resources/jsondata/visite_test.json \
    resources/style/scelta.css \
    resources/config.json \
    .gitignore
