#ifndef COVIDMANAGER_CONTROLLER_H
#define COVIDMANAGER_CONTROLLER_H

#include <QObject>
#include "view/gestionale_vax.h"
#include "view/scelta_gestionale.h"
#include "view/gestionale_tamponi.h"
#include "view/mainwindow.h"
#include "view/manager.h"

class Controller: public QObject {
    Q_OBJECT
private:
    MainWindow* mainWindow;
    Manager* manager;
    Scelta_Gestionale* sceltaGestionale;
    Gestionale_Vax* gestionaleVax;
    Gestionale_Tamponi* gestionaleTamponi;


public:
    explicit Controller(QObject *parent = nullptr);

public slots:
    void openGestionaleVax(OperatoreSanitario*) const;
    void openGestionaleTamponi(OperatoreSanitario*) const;
    void tornaIndietroVax() const;
    void tornaIndietroTamp() const;
    void accedi() const;
    void accediOS(OperatoreSanitario*) const;
    void updateListaVisite(bool, bool, QString, int);
    void updateListaVisiteVax(bool, bool, QString, int);
    void update_esito(int, bool, int , double);
    void update_form();
    void update_formVax();
    void update_paziente(QString,QString,QString);
    void update_pazienteVax(QString, QString, QString, QString, QString, QString, Data, HealthData);
    void add_visita_extra(Vaccinazione);

};


#endif //COVIDMANAGER_CONTROLLER_H
