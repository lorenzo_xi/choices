#include "Controller.h"

Controller::Controller(QObject *parent): QObject(parent),mainWindow(new MainWindow),manager(new Manager), sceltaGestionale(new Scelta_Gestionale), gestionaleVax(new Gestionale_Vax), gestionaleTamponi(new Gestionale_Tamponi) {
    connect(sceltaGestionale,SIGNAL(signGestionaleVax(OperatoreSanitario*)),this,SLOT(openGestionaleVax(OperatoreSanitario*)));
    connect(sceltaGestionale, SIGNAL(signGestionaleTamp(OperatoreSanitario*)), this, SLOT(openGestionaleTamponi(OperatoreSanitario*)));
    connect(gestionaleVax, SIGNAL(signIndietro()), this, SLOT(tornaIndietroVax()));
    connect(gestionaleTamponi, SIGNAL(signIndietro()), this, SLOT(tornaIndietroTamp()));
    connect(mainWindow, SIGNAL(signAccedi()), this, SLOT(accedi()));
    connect(manager, SIGNAL(login_operatore(OperatoreSanitario*)), this, SLOT(accediOS(OperatoreSanitario*)));

    connect(gestionaleTamponi->getbarraricerca(), SIGNAL(send_query(bool, bool, QString, int)), this, SLOT(updateListaVisite(bool, bool, QString, int)));
    connect(gestionaleVax->getBarraRicerca(), SIGNAL(send_query(bool, bool, QString, int)),this, SLOT(updateListaVisiteVax(bool, bool, QString, int)));

    connect(gestionaleTamponi->getform(), SIGNAL(paziente_successivo()), this, SLOT(update_form()));
    connect(gestionaleTamponi->getform(), SIGNAL(aggiorna_paziente(QString,QString,QString)), this, SLOT(update_paziente(QString, QString, QString)));
    connect(gestionaleVax->getform(), SIGNAL(aggiorna_paziente(QString, QString, QString, QString, QString, QString, Data, HealthData)), this, SLOT(update_pazienteVax(QString, QString, QString, QString, QString, QString, Data, HealthData)));

    connect(gestionaleVax->getform(), SIGNAL(paziente_successivo()), this, SLOT(update_formVax()));
    connect(gestionaleVax->getEmptyForm(), SIGNAL(send_visita_extra(Vaccinazione)), this, SLOT(add_visita_extra(Vaccinazione)));

    mainWindow->show();
}

void Controller::openGestionaleVax(OperatoreSanitario* op) const {
    gestionaleVax->show();
    gestionaleVax->setOperatoreSanitario(op);
    sceltaGestionale->close();
}

void Controller::openGestionaleTamponi(OperatoreSanitario* op) const {
    gestionaleTamponi->show();
    gestionaleTamponi->setOperatoreSanitario(op);
    sceltaGestionale->close();
}

void Controller::tornaIndietroVax() const {
    sceltaGestionale->show();
    gestionaleVax->close();
}

void Controller::tornaIndietroTamp() const{
    sceltaGestionale->show();
    gestionaleTamponi->close();
}
void Controller::accedi() const {
    manager->show();
    mainWindow->close();
}
void Controller::accediOS(OperatoreSanitario* op) const {
    if(op){
        sceltaGestionale->setOperatoreSanitario(op);

        if(Algorithm::fromEnumJobToStdString(op->getProfessione()) == "Infermiere"){
            sceltaGestionale->nascondi_pulsante_vaccini();
        }
    }
    sceltaGestionale->show();
    manager->close();
}



void Controller::updateListaVisite(bool vaxortest, bool erogazione, QString cf, int esito) {
    gestionaleTamponi->getlistavisite()->updateVisite(false,erogazione,cf,esito);
}
void Controller::updateListaVisiteVax(bool vaxortest, bool erogazione, QString cf, int esito) {

    gestionaleVax->getlistavisite()->updateVisite(true, erogazione, cf, esito);
}

void Controller::update_esito(int id, bool tamponeORsierologico, int esito, double valore_siero) {
    gestionaleTamponi->getlistavisite()->updateEsito(id,tamponeORsierologico,esito,valore_siero,"visite_test.json","visite_test");
}

void Controller::update_form() {
    int old_index = gestionaleTamponi->get_indice_visite();
    gestionaleTamponi->set_indice_visite( old_index+1);
    gestionaleTamponi->getlistavisite()->updateStato(old_index,true,gestionaleTamponi->get_logged_user(),"visite_test.json","visite_test");
    gestionaleTamponi->getform()->next_visita(gestionaleTamponi->get_indice_visite());
    Algorithm::setIndex(false,gestionaleTamponi->get_indice_visite());
}

void Controller::update_paziente(QString cf, QString email, QString tel) {
    gestionaleTamponi->getlistavisite()->updatePaziente(gestionaleTamponi->get_indice_visite(),cf,email,tel,"visite_test.json","visite_test");
}

void Controller::update_formVax() {
    int old_index = gestionaleVax->get_indice_visite();
    gestionaleVax->set_indice_visite( old_index+1);
    gestionaleVax->getlistavisite()->updateStatoVax(old_index,true,gestionaleVax->get_logged_user(),"visite_vax.json","visite_vax");
    gestionaleVax->getform()->next_visita(gestionaleVax->get_indice_visite());
    Algorithm::setIndex(true,gestionaleVax->get_indice_visite());
}

void Controller::update_pazienteVax(QString nome, QString cognome, QString cf, QString email, QString recapito, QString sesso, Data dataN, HealthData hd){
    gestionaleVax->getlistavisite()->updatePazienteVax(gestionaleVax->get_indice_visite(), nome, cognome, cf, email, recapito, "visite_vax.json", "visite_vax", sesso, dataN, hd);
}

void Controller::add_visita_extra(Vaccinazione v) {
    v.setID(gestionaleVax->getlistavisite()->getListaSize());
    v.set_riferimento(*gestionaleVax->get_logged_user());
    gestionaleVax->getlistavisite()->add_visita_vax(v);
}
