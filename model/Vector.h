#ifndef COVIDMANAGER_CONTAINER_H
#define COVIDMANAGER_CONTAINER_H

#include <iostream>

template <class T>
class Vector {
private:

    T* vect;
    unsigned int size;
    unsigned int capacity;

public:

    //costruttore
    Vector(unsigned int=0, const T& =T());

    //costruttore di copia
    Vector(const Vector &);

    //distruttore
    ~Vector();

    //operatori
    Vector &operator=(const Vector &);

    T& operator[](unsigned int);

    const T& operator[](unsigned int) const;

    bool operator==(const Vector &v) const;

    bool operator!=(const Vector &v) const;

    //metodi
    bool empty() const;

    unsigned int getSize() const;

    unsigned int getCapacity() const;

    void resize(unsigned int, const T&);

    void reserve(unsigned int);

    T& front();

    const T& front() const;

    T& back();

    const T& back() const;

    T &at(unsigned int);

    const T &at(unsigned int) const;

    void push_back(const T &);

    void pop_back();

    void clear();

    void del();

    static unsigned int max_size();

    class iterator {
        friend class Vector<T>;

    private: // const_iterator indefinito IFF ptr==nullptr & past_the_end==false
        T* ptr;
        bool past_the_end;

        // convertitore "privato" nodo* => const_iterator
        iterator(T* t=T(), bool p_t_e = false) : ptr(t), past_the_end(p_t_e) {}

    public:

        iterator() : ptr(nullptr), past_the_end(false) {}

        T& operator*() const;

        T& operator[](unsigned int dist) const;

        T* operator->() const;

        iterator& operator+=(int);

        iterator& operator-=(int);

        iterator operator+(int) const;

        iterator operator-(int) const;

        iterator& operator++();

        iterator operator++(int);

        iterator& operator--();

        iterator operator--(int);

        bool operator<(const iterator& it) const;

        bool operator>(const iterator& it) const;

        bool operator<=(const iterator& it) const;

        bool operator>=(const iterator& it) const;

        bool operator==(const iterator& it) const;

        bool operator!=(const iterator& it) const;

    };

    class const_iterator {
        friend class Vector<T>;

    private:
        const T* ptr;
        bool past_the_end;

        // convertitore "privato" nodo* => const_iterator
        const_iterator(T* t=T(), bool p_t_e = false) : ptr(t), past_the_end(p_t_e) {}

    public:

        const_iterator() : ptr(nullptr), past_the_end(false) {}

        const T& operator*() const;

        const T& operator[](unsigned int) const;

        const T* operator->() const;

        const_iterator& operator+=(int) ;

        const_iterator& operator-=(int);

        const_iterator operator+(int) const;

        const_iterator operator-(int) const;

        const_iterator& operator++();

        const_iterator operator++(int);

        const_iterator& operator--();

        const_iterator operator--(int);

        bool operator<(const const_iterator& it) const;

        bool operator>(const const_iterator& it) const;

        bool operator<=(const const_iterator& it) const;

        bool operator>=(const const_iterator& it) const;

        bool operator==(const const_iterator& it) const;

        bool operator!=(const const_iterator& it) const;

    };

    iterator insert(const const_iterator&, const T&);

    iterator insert(const const_iterator&, unsigned int, const T&);

    iterator erase(const_iterator);

    iterator erase(const_iterator, const_iterator);

    iterator begin();

    iterator end();

    const_iterator cbegin() const;

    const_iterator cend() const;
};


/* === V E C T O R === */

//Costruttore a zero, uno, due parametri
/*  Costruttore a zero parametri: size=capacity=vect=0, nessun elemento istanziato
 *  Costruttore a un parametro  : size=capacity=n, vect= new T[size], n elementi istanziati con costruttore di default T()
 *  Costruttore a due parametri : size=capacity=n, vect=new T[size], n elementi istanziati con assegnazione da secondo parametro
 */
template<class T>
Vector<T>::Vector(unsigned int n, const T& t): vect(n ? new T[n] : new T[1]), size(n), capacity(n ? n : 1) {
    for(unsigned int i=0; i<n; i++){
        vect[i] = t;
    }
}

//Costruzione di copia
/*  sse v.capacity != 0 allora vect = new T[v.capacity], size=v.size, capacity=v.capacity; altrimenti vect=size=capacity=0
 */
template<class T>
Vector<T>::Vector(const Vector & v):vect(new T[v.capacity]), size(v.size), capacity(v.capacity){
    for(unsigned int i=0; i<size; i++){
        vect[i] = v.vect[i];
    }
}

//Distruzione
template<class T>
Vector<T>::~Vector(){
    delete[] vect;
}

//Assegnazione
template<class T>
Vector<T>& Vector<T>::operator=(const Vector& v) {
    if(this != &v) {
        delete vect;
        size = v.size;
        capacity = v.capacity;
        vect = new T[capacity];

        for(int i=0; i<size; i++){
            vect[i] = v.vect[i];
        }
    }
    return *this;
}

//operator []
template<class T>
T& Vector<T>::operator[](unsigned int position) {
    return vect[position];
}

//operator [] const
template<class T>
const T& Vector<T>::operator[](unsigned int position) const {
    return vect[position];
}

//operator ==
template<class T>
bool Vector<T>::operator==(const Vector& v) const {
    bool ok = (size == v.size);
    int i=0;
    while(i<capacity && ok){
        ok = vect[i]==v[i];
        i++;
    }
    return ok;
}

//operator !=
template<class T>
bool Vector<T>::operator!=(const Vector& v) const {
    return !operator==(v);
}

/* === V E C T O R - methods === */
template<class T>
bool Vector<T>::empty() const{
    return (size==0);
}

template<class T>
unsigned int Vector<T>::getSize() const{
    return size;
}

template<class T>
unsigned int Vector<T>::getCapacity() const{
    return capacity;
}

template<class T>
unsigned int Vector<T>::max_size() {
    return 2048;
}

template<class T>
void Vector<T>::reserve(unsigned int new_capacity){
    if(new_capacity > max_size())
        throw std::length_error("Overflow ! new_capacity > max_sixe consentito");

    if(new_capacity > capacity) {
        T* aux = new T[new_capacity];
        for(unsigned int i = 0; i < size; i++)
            aux[i] = vect[i];
        delete[] vect;
        vect = aux;
        capacity = new_capacity;
    }else{
        throw std::invalid_argument( "new_capacity < capacity: nessun operazione fatta" );
    }
}

template<class T>
T& Vector<T>::front() {
    return vect[0];
}

template<class T>
const T& Vector<T>::front() const {
    return vect[0];
}

template<class T>
T& Vector<T>::back() {
    return vect[size-1];
}

template<class T>
const T& Vector<T>::back() const {
    return vect[size-1];
}

template<class T>
T& Vector<T>::at(unsigned int position) {
    if(position > size){
       throw std::out_of_range("Out of range");
    }
    return vect[position];
}

template<class T>
const T& Vector<T>::at(unsigned int position) const {
    if(position > size)
      throw std::out_of_range("Out of range");

    return vect[position];
}

template<class T>
void Vector<T>::push_back(const T& t) {
    if(size == capacity) {
        reserve(capacity * 2);
    }
    vect[size++] = t;

}

template<class T>
void Vector<T>::pop_back() {
    if(size){
        size--;
    }
}

template<typename T>
void Vector<T>::resize(unsigned int newSize, const T& t) {
    if(newSize < size)
        size = newSize;
    else if(newSize <= capacity){
        unsigned int diff = newSize - size;
        for(unsigned int i = 0; i < diff; i++)
            vect[size+1] = t;
        size = newSize;
    } else {
        T* aux = new T[newSize];

        for(unsigned int i = 0; i < newSize; i++) {
            if(i < size)
                aux[i] = vect[i];
            else aux[i] = t;
        }

        delete[] vect;
        size = capacity = newSize;
        vect = aux;
    }
}

template<class T>
void Vector<T>::clear() {
    delete[] vect;
    size = 0;
    capacity = 1;
    vect = new T[1];
}

template<class T>
void Vector<T>::del() { // usata solo come test
    delete[] vect;
}

template<class T>
typename Vector<T>::iterator Vector<T>::begin() {
    return Vector<T>::iterator(vect);
}

template<class T>
typename Vector<T>::iterator Vector<T>::end() {
    return Vector<T>::iterator(vect+size);
}

template<class T>
typename Vector<T>::const_iterator Vector<T>::cbegin() const {
    return Vector<T>::const_iterator(vect);
};


template<class T>
typename Vector<T>::const_iterator Vector<T>::cend() const {
    return Vector<T>::const_iterator(vect+size);
}


template<class T>
typename Vector<T>::iterator Vector<T>::insert(const Vector::const_iterator & pos, const T & t) {
    return insert(pos, 1, t);
}


template<class T>
typename Vector<T>::iterator Vector<T>::insert(const Vector::const_iterator & position, unsigned int num, const T & t) {
    int pos = static_cast<unsigned int>(position.ptr - vect);

    if(size + num > capacity) {
        capacity = (size + num) * 2;
        T* aux = new T[capacity];

        for(unsigned int i = 0; i < pos; i++)
            aux[i] = vect[i];

        for(unsigned int i = 0; i < num; i++)
            aux[pos+i] = t;

        for(unsigned int i = pos; i < size; i++)
            aux[i+1] = vect[i];

        delete[] vect;
        size++;
        vect = aux;
        return Iterator(aux+pos);
    }

    for(int i = size-1; i >= pos; i--)
        vect[i+num] = vect[i];

    for(unsigned int i = 0; i < num; i++)
        vect[pos+i] = t;
    size++;

    return iterator(vect+pos);
}

template<class T>
typename Vector<T>::iterator Vector<T>::erase(Vector::const_iterator pos) {
    return erase(pos, pos+1);
}

template<class T>
typename Vector<T>::iterator Vector<T>::erase(Vector<T>::const_iterator first, Vector<T>::const_iterator last) {
    unsigned int number = static_cast<unsigned int>(last.ptr - first.ptr);
    unsigned int lastPosition = static_cast<unsigned int>(last._ptr - vect);
    unsigned int firstPosition = static_cast<unsigned int>(first._ptr - vect);

    for(unsigned int i = lastPosition; i < vect; i++) {
        vect[i-number] = vect[i];
    }

    size -= number;
    return Iterator(vect+firstPosition);
}

/*  === ITERATOR  === */

template<class T>
T& Vector<T>::iterator::operator*() const {
    return *ptr;
}

template<class T>
T& Vector<T>::iterator::operator[](unsigned int pos) const {
    return *(ptr + pos);
}

template<class T>
T* Vector<T>::iterator::operator->() const {
    return ptr;
}

template<class T>
typename Vector<T>::iterator& Vector<T>::iterator::operator+=(int pos) {
    ptr += pos;
    return *this;
}


template<class T>
typename Vector<T>::iterator& Vector<T>::iterator::operator-=(int pos) {
    ptr -= pos;
    return *this;
}

template<class T>
typename Vector<T>::iterator Vector<T>::iterator::operator+(int pos) const {
    return iterator(ptr+pos);
}

template<class T>
typename Vector<T>::iterator Vector<T>::iterator::operator-(int pos) const {
    return iterator(ptr-pos);
}

template<class T>
typename Vector<T>::iterator& Vector<T>::iterator::operator++() {
    ptr++;
    return *this;
}

template<class T>
typename Vector<T>::iterator Vector<T>::iterator::operator++(int) {
    auto it = *this;
    ptr++;
    return it;
}

template<class T>
typename Vector<T>::iterator& Vector<T>::iterator::operator--() {
    ptr--;
    return *this;
}

template<class T>
typename Vector<T>::iterator Vector<T>::iterator::operator--(int) {
    auto it = *this;
    ptr--;
    return it;
}

template<class T>
bool Vector<T>::iterator::operator<(const iterator& it) const {
    return *ptr < *it.ptr;
}

template<class T>
bool Vector<T>::iterator::operator>(const iterator& it) const {
    return *ptr > *it.ptr;
}

template<class T>
bool Vector<T>::iterator::operator<=(const iterator& it) const {
    return *ptr <= *it.ptr;
}

template<class T>
bool Vector<T>::iterator::operator>=(const iterator& it) const {
    return *ptr >= *it.ptr;
}

template<class T>
bool Vector<T>::iterator::operator==(const iterator& it) const {
    return ptr == it.ptr;
}

template<class T>
bool Vector<T>::iterator::operator!=(const iterator& it) const {
    return ptr != it.ptr;
}


/* === CONST ITERATOR === */

template<class T>
const T& Vector<T>::const_iterator::operator*() const {
    return *ptr;
}

template<class T>
const T& Vector<T>::const_iterator::operator[](unsigned int pos) const {
    return *(ptr + pos);
}

template<class T>
const T* Vector<T>::const_iterator::operator->() const {
    return *ptr;
}

template<class T>
typename Vector<T>::const_iterator& Vector<T>::const_iterator::operator+=(int pos) {
    ptr += pos;
    return *this;
}

template<class T>
typename Vector<T>::const_iterator& Vector<T>::const_iterator::operator-=(int pos) {
    ptr -= pos;
    return *this;
}

template<class T>
typename Vector<T>::const_iterator Vector<T>::const_iterator::operator+(int pos) const {
    return iterator(ptr+pos);
}

template<class T>
typename Vector<T>::const_iterator Vector<T>::const_iterator::operator-(int pos) const {
    return iterator(ptr-pos);
}

template<class T>
typename Vector<T>::const_iterator& Vector<T>::const_iterator::operator++() {
    ptr++;
    return *this;
}

template<class T>
typename Vector<T>::const_iterator Vector<T>::const_iterator::operator++(int) {
    auto it = *this;
    ptr++;
    return it;
}

template<class T>
typename Vector<T>::const_iterator& Vector<T>::const_iterator::operator--() {
    ptr--;
    return *this;
}

template<class T>
typename Vector<T>::const_iterator Vector<T>::const_iterator::operator--(int) {
    auto it = *this;
    ptr--;
    return it;
}

template<class T>
bool Vector<T>::const_iterator::operator<(const const_iterator& it) const {
    return ptr < it.ptr;
}

template<class T>
bool Vector<T>::const_iterator::operator>(const const_iterator& it) const {
    return ptr > it.ptr;
}

template<class T>
bool Vector<T>::const_iterator::operator<=(const const_iterator& it) const {
    return ptr <= it.ptr;
}

template<class T>
bool Vector<T>::const_iterator::operator>=(const const_iterator& it) const {
    return ptr >= it.ptr;
}

template<class T>
bool Vector<T>::const_iterator::operator==(const const_iterator& it) const {
    return ptr == it.ptr;
}

template<class T>
bool Vector<T>::const_iterator::operator!=(const const_iterator& it) const {
    return ptr != it.ptr;
}

#endif //COVIDMANAGER_CONTAINER_H
