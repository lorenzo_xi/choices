#ifndef COVIDMANAGER_DEEPPTR_H
#define COVIDMANAGER_DEEPPTR_H

#include "model/gerarchia/Base.h"

template<class T = Base*>
class DeepPtr{

private:
    T* dptr;
public:
    DeepPtr(T* =nullptr);
    DeepPtr(const DeepPtr&);
    DeepPtr& operator=(const DeepPtr&);
    T* operator->() const;
    ~DeepPtr();

    T* get();
    T& operator*();
    T* operator->();

    bool operator==(const DeepPtr&) const;
    bool operator!=(const DeepPtr&) const;
    bool operator>(const DeepPtr&) const;
    bool operator<(const DeepPtr&) const;
};

template <class T>
DeepPtr<T>::DeepPtr(T* item): dptr(item){}

template <class T>
DeepPtr<T>::DeepPtr(const DeepPtr<T>& ptr) {
    if (!ptr)
        dptr = nullptr;
    else {
        dptr = ptr.dptr->clone();
    }
}

template <class T>
DeepPtr<T>& DeepPtr<T>::operator=(const DeepPtr& ptr){

    if(this != &ptr){
        if(dptr){
            delete dptr;
        }
        dptr = ptr.dptr->clone();
    }
    return *this;
}


template <class T>
T* DeepPtr<T>::operator->() const{
    return dptr;
}

template <class T>
DeepPtr<T>::~DeepPtr(){
    if(dptr) {
        delete dptr;
    }
}

template <class T>
bool DeepPtr<T>::operator==(const DeepPtr& dptr) const{
    return *dptr==*(dptr.pted);
}

template <class T>
bool DeepPtr<T>::operator!=(const DeepPtr& dptr) const{
    return *dptr!=*(dptr.pted);
}

template<class T>
bool DeepPtr<T>::operator<(const DeepPtr& dptr) const{
    return *dptr<*(dptr.pted);
}

template<class T>
bool DeepPtr<T>::operator>(const DeepPtr& dptr) const{
    return *dptr>*(dptr.pted);
}

template<class T>
T* DeepPtr<T>::get() {
    return dptr;
}

template<class T>
T& DeepPtr<T>::operator*() {
    return *dptr;
}

template<class T>
T* DeepPtr<T>::operator->() {
    return dptr;
}



#endif //COVIDMANAGER_DEEPPTR_H