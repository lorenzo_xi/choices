#ifndef COVIDMANAGER_DATA_H
#define COVIDMANAGER_DATA_H
#include <iostream>
#include "Type.h"
#include <iostream>
#include <QtCore>
#include <regex>

class Data: virtual public Type{
private:
    int giorno;
    int mese;
    int anno;
public:

    //costruttori
    Data(int=0, int=0, int=0);
    Data(const std::string&);

    //get
    int getGiorno() const;
    int getMese() const;
    int getAnno() const;

    //operatori
    friend std::ostream& operator<<(std::ostream&, const Data&);

    //metodi virtuali
    virtual std::string stringify() const override;
};



#endif //COVIDMANAGER_DATA_H