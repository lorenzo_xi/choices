#ifndef CHOICES_TYPE_H
#define CHOICES_TYPE_H

#include <iostream>

class Type{
public:
    //metodi virtuali puri
    virtual std::string stringify() const = 0;
};

#endif //CHOICES_TYPE_H