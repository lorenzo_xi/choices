#include "HealthData.h"
#include "model/Algo/algorithm.h"

//costruttori
HealthData::HealthData(unsigned int p, EnumTypes::Risk r, bool hc, bool iv)
    :   patologie(p), rischio(r), hadCovid(hc), isVaxed(iv)
    { }

HealthData::HealthData(const std::string& str) {  //n,n,n,n;
    try{
        if(! std::regex_match(str,
                            std::regex("[0-9]{1,2}[','][0-9][','][0-1][','][0-1][\";\"]"))){
            throw QJsonParseError::ParseError();
        }

        patologie=str[0] - '0'; //conversione char => int
        rischio = static_cast<EnumTypes::Risk>(str[2] - '0');
        hadCovid=str[4] - '0';
        isVaxed =str[6] - '0';

    } catch(QJsonParseError::ParseError()) {
        HealthData();
        std::cerr<<"errore nel formato stringa cartella clinica";
    }
}

//get
int HealthData::getPatologie() const{
    return patologie;
}

EnumTypes::Risk HealthData::getRischio() const{
    return rischio;
}

bool HealthData::getHadCovid() const{
    return hadCovid;
}

bool HealthData::getIsVaxed() const{
    return isVaxed;
}

//set
void HealthData::setPatologie(int p){
    patologie=p;
}

void HealthData::setRischio(EnumTypes::Risk r){
    rischio=r;
}

void HealthData::setHadCovid(bool hc){
    hadCovid=hc;
}

void HealthData::setIsVaxed(bool iv){
    isVaxed=iv;
}

std::string HealthData::stringify() const{  //"3,4,0,1"
    return std::to_string(patologie)+","+
            (rischio==EnumTypes::Basso ? "1" : (rischio==EnumTypes::Medio ? "2":"3"))
    +","+std::to_string(hadCovid)+","+std::to_string(isVaxed)+";";
}
