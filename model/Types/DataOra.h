#ifndef COVIDMANAGER_DATAORA_H
#define COVIDMANAGER_DATAORA_H
#include "Ora.h"
#include "Data.h"

class DataOra: public Ora, Data {
public:
    //costruttori
    DataOra(int, int, int, int, int, int);
    DataOra(const Data&, const Ora&);
    DataOra(const std::string& s);

    //operatori
    friend std::ostream& operator<<(std::ostream&, const DataOra&);

    //metodi
    Data getDataFromDataOraString(std::string) const;
    Ora getOraFromDataOraString(std::string)   const;

    //metodi virtuali
    virtual std::string stringify() const override ;

};


#endif //COVIDMANAGER_DATAORA_H
