#ifndef COVIDMANAGER_HEALTHDATA_H
#define COVIDMANAGER_HEALTHDATA_H

#include "EnumTypes.h"
#include <iostream>
#include "Type.h"
#include "../Types/EnumTypes.h"
#include <iostream>
#include <sstream>
#include <QtGui>
#include <string>
#include "../Vector.h"
#include <regex>

class HealthData: public Type {       //dati medici
private:

    unsigned int        patologie;  //numero di patologie
    EnumTypes::Risk     rischio;    //rischio di morte per Covid-19
    bool                hadCovid;   //infezione è già stata contrata (se si==true, altrimenti==false)
    bool                isVaxed;    //indica se è già stata effettuata una vaccinazione completa (se si==true, altrimenti==false)

public:

    //costruttori
    HealthData(unsigned int= 0, EnumTypes::Risk=EnumTypes::Basso, bool= false, bool= false);
    HealthData(const std::string&);  //n,n,n,n;

    //get
    int getPatologie() const;
    EnumTypes::Risk getRischio() const;
    bool getHadCovid() const;
    bool getIsVaxed() const;

    //set
    void setPatologie(int);
    void setRischio(EnumTypes::Risk);
    void setHadCovid(bool);
    void setIsVaxed(bool);

    //metodi virtuali
    virtual std::string stringify() const override;
};

#endif //COVIDMANAGER_HEALTHDATA_H