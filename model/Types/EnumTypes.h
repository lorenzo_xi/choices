#ifndef COVIDMANAGER_TYPES_H
#define COVIDMANAGER_TYPES_H


namespace EnumTypes{

    enum Job : short{
        Doctor  =   1,
        Nurse   =   2
    };

    enum Vax : short{
        NonSpecificato = 0,
        Pfizer      =   1,
        Moderna     =   2,
        AstraZeneca =   3,
        JandJ       =   4,
        Novavax     =   5,
        SputnikV    =   6,
        SinoVac     =   7,
    };

    enum Tampone: short{
        Antigenico  =   1,
        Molecolare  =   2,
    };

    enum Risk: short{
        Basso = 1,
        Medio = 2,
        Alto  = 3,
    };
}

#endif //COVIDMANAGER_TYPES_H
