#include "Data.h"


Data::Data(int g, int m, int a): giorno (g), mese(m), anno(a){}

Data::Data(const std::string& s) {//14/03/2000 or 14-03-2000
    try{

        std::string g1,g2,m1,m2,a1,a2,a3,a4;
        g1=s[0];
        g2=s[1];
        m1=s[3];
        m2=s[4];
        a1=s[6];
        a2=s[7];
        a3=s[8];
        a4=s[9];

        giorno = std::stoi(g1)*10 + std::stoi(g2);
        mese = std::stoi(m1)*10 + std::stoi(m2);
        anno = std::stoi(a1)*1000 + std::stoi(a2)*100 + std::stoi(a3)*10 + std::stoi(a4);


    } catch (...) {
        giorno=mese=anno=0;
        //std::cerr << "errore nel processo di elaborazione della stringa \'Data\'" << std::endl;
    }

}

std::string Data::stringify() const{

    std::string gg = (giorno<10?"0":"") + std::to_string(giorno);
    std::string mm = (mese<10?"0":"") + std::to_string(mese);
    std::string yy = std::to_string(anno);

    std::string result = gg+"/"+mm+"/"+yy;

    return result;
}

int Data::getGiorno() const{
    return giorno;
}

int Data::getMese() const{
    return mese;
}

int Data::getAnno() const{
    return anno;
}

std::ostream& operator<<(std::ostream& os, const Data& d){
    os << d.giorno << '/' << d.mese  << '/' << d.anno;
    return os;
}
