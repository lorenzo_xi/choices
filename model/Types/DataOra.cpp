#include "DataOra.h"

DataOra::DataOra(int o, int mi, int s, int g, int a, int m): Ora(o,mi,s), Data(g,m,a){}
DataOra::DataOra(const Data & d, const Ora & o):Ora(o), Data(d)  {}

std::string DataOra::stringify() const{
    return Data::stringify() + ";" + Ora::stringify();
}

std::ostream& operator<<(std::ostream& os, const DataOra& d_o){
    os << d_o.getGiorno() << '/' << d_o.getMese()  << '/' << d_o.getAnno() << ';' << d_o.getOre() << ':' << d_o.getMinuti() << ':' << d_o.getSecondi();
    return os;
}

DataOra::DataOra(const std::string& d_o): Ora(getOraFromDataOraString(d_o)),Data(getDataFromDataOraString(d_o)) {
    try {
        if (!std::regex_match(d_o, std::regex("[0-3][0-9][\\/ | \\-][0-1][0-9][\\/ | \\-][0-9]{2,4}[;][0-2][0-9]:[0-5][0-9]:[0-5][0-9]"))) {
            throw QJsonParseError::ParseError();
        }

    }catch (QJsonParseError::ParseError()) {
        std::cerr<<"errore nel formato stringa" << std::endl;
    } catch (...) {
        std::cerr<<"errore nel processo di elaborazione della stringa \'DataOra\'" << std::endl;
    }
}

Data DataOra::getDataFromDataOraString(std::string d_o) const{
    size_t found;
    Data d;
    if ((found = d_o.find(";")) != std::string::npos){
        d = Data(d_o.substr(0,found));
    }else{
        throw QJsonParseError::ParseError();
    }
    return d;
}

Ora DataOra::getOraFromDataOraString(std::string d_o) const{
    size_t found;
    Ora o;
    if ((found = d_o.find(";")) != std::string::npos){
        o = Ora(d_o.substr(found+1, std::string::npos));
        o.stringify();
    }else{
        throw QJsonParseError::ParseError();
    }
    return o;
}
