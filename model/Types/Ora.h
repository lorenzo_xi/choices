#ifndef COVIDMANAGER_ORA_H
#define COVIDMANAGER_ORA_H

#include "Type.h"
#include <regex>
#include <QtCore>
#include <iostream>

class Ora: virtual public Type{
private:
    int sec;
public:

    //costruttori
    Ora();
    Ora(unsigned int);
    Ora(unsigned int, unsigned int);
    Ora(unsigned int, unsigned int, unsigned int);
    Ora(const std::string&);

    //get
    int getOre() const;
    int getMinuti() const;
    int getSecondi() const;

    //operatori
    friend std::ostream& operator<<(std::ostream&, const Ora&);

    //metodi virtuali
    virtual std::string stringify() const override ;
};


#endif //COVIDMANAGER_ORA_H