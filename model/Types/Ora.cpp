#include "Ora.h"

Ora::Ora(){
    sec=0;
};

Ora::Ora(unsigned int o){
    if (o>23){
        sec=0;
    }else{
        sec = o * 3600;
    }
};

Ora::Ora(unsigned int o, unsigned int m){
    if (o>23 ||m>59){
        sec=0;
    }else{
        sec = o * 3600 + m * 60;
    }
};

Ora::Ora(unsigned int o, unsigned int m, unsigned int s){
    if (o>23 || m>59 || s>59){
        sec=0;
    }else{
        sec = o * 3600 + m * 60 + s;
    }
};

Ora::Ora(const std::string& o) {
    try{
        if(! std::regex_match(o,std::regex("[0-2][0-9]:[0-5][0-9]:[0-5][0-9]"))){
            throw QJsonParseError::ParseError();
        }

        std::string o1,o2,m1,m2,s1,s2;
        o1=o[0];
        o2=o[1];
        m1=o[3];
        m2=o[4];
        s1=o[6];
        s2=o[7];

        sec = (std::stoi(o1)*10 + std::stoi(o2))*3600 + (std::stoi(m1)*10 + std::stoi(m2))*60 + std::stoi(s1)*10 + std::stoi(s2);
    } catch (QJsonParseError::ParseError()) {
        std::cerr<<"errore nel formato stringa" << std::endl;
    } catch (...) {
        std::cerr<<"errore nel processo di elaborazione della stringa \'Ora\'" << std::endl;
    }

}

int Ora::getOre() const {
    return sec/3600;
};
int Ora::getMinuti() const {
    return (sec/60) %60;
};
int Ora::getSecondi() const  {
    return sec % 60;
};

std::string Ora::stringify() const {
    return (getOre() < 10 ? "0" : "")       +
           std::to_string(getOre())     + ":" +
           (getMinuti() < 10 ? "0" : "")    +
           std::to_string(getMinuti())  + ":" +
           (sec < 10 ? "0" : "")            +
           std::to_string(getSecondi());

}

std::ostream& operator<<(std::ostream& os, const Ora& o){
os << o.getOre() << ':' << o.getMinuti()  << ':' << o.getSecondi();
return os;
}
