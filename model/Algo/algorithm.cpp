#include "algorithm.h"

namespace Algorithm {

    //  ENUM => STDSTRING


    std::string fromEnumJobToStdString(const EnumTypes::Job &en) {
        std::string s = "";

        if (en == EnumTypes::Doctor) {
            s = "Dottore";
        } else {
            s = "Infermiere";
        }
        return s;
    }

    EnumTypes::Job fromIntToEnumJob(int n) {
        if (n == 1) {
            return EnumTypes::Doctor;
        }
        return EnumTypes::Nurse;
    }

    EnumTypes::Vax fromIntToEnumVax(int n) {
        EnumTypes::Vax v;
        switch(n){

        case 0:
            v = EnumTypes::Pfizer;
            break;

        case 1:
            v = EnumTypes::Moderna;
            break;

        case 2:
            v = EnumTypes::AstraZeneca;
            break;

        case 3:
            v = EnumTypes::JandJ;
            break;

        case 4:
            v = EnumTypes::Novavax;
            break;

        case 5:
            v = EnumTypes::SputnikV;
            break;

        case 6:
            v = EnumTypes::SinoVac;
            break;


        default:
            v = EnumTypes::AstraZeneca;
            break;
        }
        return v;
    }

    std::string fromEnumVaxToStdString(const EnumTypes::Vax &en) {
        std::string s;
        switch (en) {
            case 1:
                s = "Pfizer";
                break;

            case 2:
                s = "Moderna";
                break;

            case 3:
                s = "Astrazeneca";
                break;

            case 4:
                s = "JandJ";
                break;

            case 5:
                s = "NovaVax";
                break;

            case 6:
                s = "SputnikV";
                break;

            case 7:
                s = "SinoVac";
                break;

            default:
                s = "Non specificato";
                break;
        }
        return s;
    }

    EnumTypes::Vax fromStringToEnum(std::string s){
        if(s == "Pfizer"){
            return EnumTypes::Vax::Pfizer;

        }else if(s == "Moderna"){
            return EnumTypes::Vax::Moderna;

        }else if(s == "Astrazeneca"){
            return EnumTypes::Vax::AstraZeneca;

        }else if(s == "JandJ"){
            return EnumTypes::Vax::JandJ;

        }else if(s == "Novavax"){
            return EnumTypes::Vax::Novavax;

        }else if(s == "SputnikV"){
            return EnumTypes::Vax::SputnikV;

        }else if(s == "SinoVac"){
            return EnumTypes::Vax::SinoVac;

        }else{
            return EnumTypes::Vax::NonSpecificato;
        }
    }

    std::string fromEnumTampToStdString(const EnumTypes::Tampone &en) {
        int n = static_cast<int>(en);
        std::string s;

        switch (n) {
            case 1:
                s = "Rapido";
                break;

            case 2:
                s = "Molecolare";
                break;

            default:
                s = "Non specificato";
                break;
        }
        return s;
    }

    std::string fromEnumRiskToStdString(const EnumTypes::Risk &en) {
        int n = static_cast<int>(en);
        std::string s ="";

        switch (n) {
            case 1:
                s = "Basso";
                break;

            case 2:
                s = "Medio";
                break;

            case 3:
                s = "Alto";
                break;

            default:
                s = "Non specificato";
                break;
        }
        return s;
    }

    EnumTypes::Risk fromStdStringToEnumRisk(const std::string& en){
        int to_ret;
        if(en=="Basso"){
            to_ret=1;
        }
        if (en=="Medio"){
            to_ret=2;
        }
        if (en=="Alto"){
            to_ret=3;
        }

        EnumTypes::Risk to_ret2= static_cast<EnumTypes::Risk>(to_ret);
        return to_ret2;
    }

    double priceOfVax(const EnumTypes::Vax &en){

        int n = static_cast<int>(en);
        double price;

        switch (n) {
            case 1:
                price = 20;
                break;

            case 2:
                price = 30;
                break;

            case 3:
                price = 5;
                break;

            case 4:
                price = 15;
                break;

            case 5:
                price = 20;
                break;

            case 6:
                price = 2;
                break;

            case 7:
                price = 4;
                break;

            default:
                price = 0;
                break;
        }
        return price;
    }

    //  I/O
    void Test_readFile(Vector<QString> &v, const std::string &path, unsigned int index) {
        QFile file(QString::fromStdString(path));
        QString val;
        QJsonDocument jsonResponse;

        try {
            file.open(QFile::ReadOnly | QFile::Text);
            val = file.readAll();
            jsonResponse = QJsonDocument::fromJson(val.toUtf8());
            QJsonObject jsonObject = jsonResponse.object();
            QJsonArray jsonArray = jsonObject["visite_test"].toArray();
            unsigned int array_size = jsonArray.size();

            if (index<array_size) {
                QJsonObject tmp_obj_test = jsonArray.at(index).toObject();
                QJsonObject tmp_obj_paziente = tmp_obj_test["paziente"].toObject();

                v.push_back(tmp_obj_paziente["cf"].toString());
                v.push_back(tmp_obj_paziente["email"].toString());
                v.push_back(tmp_obj_paziente["telefono"].toString());

                if (tmp_obj_test.contains("tipo_test")) {
                    v.push_back(tmp_obj_test["tipo_test"].toString());
                } else {
                    v.push_back("3");
                }

            } else {
                throw std::out_of_range("out of range");
            }

            file.close();

        } catch (QFile::FileError) {
            throw QFile::FileError();
        } catch (QJsonParseError::ParseError) {
            throw QJsonParseError::ParseError();
        } catch (std::out_of_range) {
            throw std::out_of_range("out of range");
        }
    }

    void Vax_readFile(Vector<QString> &v, const std::string &path, unsigned int index) {
        QFile file(QString::fromStdString(path));
        QString val;
        QJsonDocument jsonResponse;
        try {
            file.open(QFile::ReadOnly | QFile::Text);
            val = file.readAll();
            jsonResponse = QJsonDocument::fromJson(val.toUtf8());
            QJsonObject jsonObject = jsonResponse.object();
            QJsonArray jsonArray = jsonObject["visite_vax"].toArray();
            unsigned int array_size = jsonArray.size();


            if (index < array_size) {

                QJsonObject tmp_obj_test = jsonArray.at(index).toObject();
                QJsonObject tmp_obj_paziente = tmp_obj_test["paziente"].toObject();

                v.push_back(tmp_obj_paziente["nome"].toString());
                v.push_back(tmp_obj_paziente["cognome"].toString());
                v.push_back(tmp_obj_paziente["cf"].toString());
                v.push_back(tmp_obj_paziente["email"].toString());
                v.push_back(tmp_obj_paziente["telefono"].toString());
                v.push_back(tmp_obj_paziente["data_nascita"].toString());
                v.push_back(tmp_obj_paziente["sesso"].toString());
                v.push_back(tmp_obj_test["azienda_produttrice"].toString());
                v.push_back(tmp_obj_paziente["cartella_clinica"].toString());


            } else {
                throw std::out_of_range("out of range");
            }

            file.close();

        } catch (QFile::FileError) {
            throw QFile::FileError();
        } catch (QJsonParseError::ParseError) {
            throw QJsonParseError::ParseError();
        } catch (std::out_of_range) {
            throw std::out_of_range("out of range");
        }
    }

    void readFile_toVectorDeepPtr(Vector<DeepPtr<Base>> &v, const std::string &path, const std::string &tipo) {

        QFile file(QString::fromStdString(path));
        QString val;
        QJsonDocument jsonResponse;

        if (!v.empty()) {
            v.clear();
        }

        try {
            file.open(QFile::ReadOnly | QFile::Text);
            val = file.readAll();
            jsonResponse = QJsonDocument::fromJson(val.toUtf8());
            QJsonObject jsonObject = jsonResponse.object();
            QJsonArray jsonArray = jsonObject[QString::fromStdString(tipo)].toArray();

            if (tipo == "operatore_sanitario") {
                for (QJsonValue value: jsonArray) {
                    QJsonObject obj = value.toObject();
                    OperatoreSanitario *c = new OperatoreSanitario(obj);
                    DeepPtr<Base> dpt(c);
                    v.push_back(dpt);
                }

            } else if (tipo == "visite_vax") {
                for (QJsonValue value: jsonArray) {
                    QJsonObject obj = value.toObject();
                    Vaccinazione *c = new Vaccinazione(obj);
                    DeepPtr<Base> dpt(c);
                    v.push_back(dpt);
                }

            } else if (tipo == "visite_test") {

                for (QJsonValue value: jsonArray) {
                    QJsonObject obj = value.toObject();

                    if (value.toObject().contains("tipo_tampone")) {
                        Tampone *c = new Tampone(obj);
                        DeepPtr<Base> dpt(c);
                        v.push_back(dpt);
                    } else {
                        Sierologico *c = new Sierologico(obj);
                        DeepPtr<Base> dpt(c);
                        v.push_back(dpt);
                    }
                }
            }

            file.close();

        } catch (QFile::FileError) {
            throw QFile::FileError();
        } catch (QJsonParseError::ParseError) {
            throw QJsonParseError::ParseError();
        }
    }

    QJsonArray readFile_toJsonArray(const std::string &path, const std::string &tipo) {

        QFile file(QString::fromStdString(path));
        QString val;
        QJsonDocument jsonResponse;
        QJsonArray jsonArray;

        try {
            file.open(QFile::ReadOnly | QFile::Text);
            val = file.readAll();
            jsonResponse = QJsonDocument::fromJson(val.toUtf8());
            QJsonObject jsonObject = jsonResponse.object();
            jsonArray = jsonObject[QString::fromStdString(tipo)].toArray();
            file.close();

        } catch (QFile::FileError) {
            throw QFile::FileError();
        } catch (QJsonParseError::ParseError) {
            throw QJsonParseError::ParseError();
        }
        return jsonArray;
    }

    void write(const std::string &path, const std::string &tipo, const QJsonArray &vobj) {
        try {
            QJsonArray jsonarray = readFile_toJsonArray(path, tipo);

            for (auto it = vobj.begin(); it != vobj.end(); ++it) {
                jsonarray.append(*it);
            }

            QJsonObject o;
            o.insert(QString::fromStdString(tipo), jsonarray);

            QFile file(QString::fromStdString(path));
            file.open(QFile::WriteOnly | QFile::Text);

            file.resize(0);

            file.write(QJsonDocument(o).toJson());
            file.close();

        } catch (QFile::FileError) {
            throw QFile::FileError();
        } catch (QJsonParseError::ParseError) {
            throw QJsonParseError::ParseError();
        }
    }

    void update(Vector<DeepPtr<Base>> &v, const std::string &path, const std::string &arrrayname) {
        try {
            QJsonArray jsonarray;

            for (auto it = v.begin(); it != v.end(); ++it) {

                QJsonObject tmp_obj;

                Vaccinazione *tmp_v = dynamic_cast<Vaccinazione *>(it->get());
                Tampone *tmp_t = dynamic_cast<Tampone *>(it->get());
                Sierologico *tmp_s = dynamic_cast<Sierologico *>(it->get());

                if (tmp_v) {
                    tmp_v->toJsonObj(tmp_obj);
                } else if (tmp_t) {
                    tmp_t->toJsonObj(tmp_obj);
                } else if (tmp_s) {
                    tmp_s->toJsonObj(tmp_obj);
                }
                jsonarray.append(tmp_obj);
            }

            QJsonObject o;
            o.insert(QString::fromStdString(arrrayname), jsonarray);

            QFile file((QString::fromStdString(path)));
            file.open(QFile::ReadWrite | QFile::Text);

            file.resize(0);

            file.write(QJsonDocument(o).toJson());
            file.close();

        } catch (QFile::FileError) {
            throw QFile::FileError();
        } catch (QJsonParseError::ParseError) {
            throw QJsonParseError::ParseError();
        }
    }

    QString get_testoPdf_test(int tipo, QString email, QString cf, QString recapito) {
        QString time = QDateTime::currentDateTime().toString("dd.MM.yyyy");

        QString testopdf = "<img src=\"resources/img/logoPDF.png\" width=\"400\" height=\"100\" align=\"center\"> <br>";

        testopdf += "<h2> Ricevuta di erogazione test COVID-19 in data " + time + "</h2> <br>";

        switch (tipo) {

            case 0:
                testopdf += "<h3> Tipologia tampone: antigenico </h3> ";
                testopdf += "<h3> Riceverà l'esito entro 1h </h3> <br> ";
                break;

            case 1:
                testopdf += "<h3> Tipologia tampone: molecolare </h3> ";
                testopdf += "<h3> Riceverà l'esito entro 2 giorni </h3> <br> ";
                break;

            case 2:
                testopdf += "<h3> Tipologia tampone: sierologioo </h3>";
                testopdf += "<h3> Riceverà l'esito entro 2 giorni </h3> <br> ";
                break;

            default:
                testopdf += "<h3> ? </h3>";
                break;

        }

        testopdf += " <p> Può vedere i risultati del suo esame sul sito covidmanager.esempio.it, nella sezione personale. </p>";
        testopdf += " <p> Per accedere utilizzi la sua email o il suo cf, riceverà le istruzioni per la password tramite SMS al suo primo login </p> <br> ";
        testopdf += " <h2> Dati personali paziente:  </h2> <p>"
                + cf + "<br> "
                + email + "<br>"
                + recapito + "</p>";
        return testopdf;
    }

    QString
    get_testoPdf_vax(QString nome, QString cognome, QString dataNascita, QString email, QString cf, QString recapito, QString vax) {
        QString time = QDateTime::currentDateTime().toString("dd.MM.yyyy");
        QString testopdf = "<img src=\"resources/img/logoPDF.png\" width=\"350\" height=\"100\" align=\"center\"> <br>";

        testopdf += "<h2> Ricevuta di erogazione vaccino anti COVID-19 in data " + time + "</h2> <br>";

        testopdf += "<p> Gentile <em>" + cognome + " " + nome + "</em> Le comunichiamo che le è stata somministrata la dose richiesta del vaccino anti COVID-19 con successo. </p> <br>";
        testopdf += "<h3> Tipo di vaccino somministrato : </h3>" "<p> " + vax + " </p>";
        testopdf += "<p> <em> Se riscontra dolore o anomalie nei seguenti giorni si rivolga tempestivamente al Pronto Soccorso </em> </p> <br>";

        testopdf += " <h3>Dati personali paziente:</h3> <p>"
                    + nome + " " + cognome + "<br>"
                    + dataNascita + "<br>"
                    + cf + "<br> "
                    + email + "<br>"
                    + recapito + "</p>";


        return testopdf;
    }

    int getIndex(bool vaxORtest) {
        QJsonArray jsonarray;
        jsonarray = readFile_toJsonArray("resources/config.json", "config");
        int ret=0;
        if(vaxORtest){
            ret = jsonarray.takeAt(0).toObject()["index_vax"].toInt();
        }else{
            ret = jsonarray.takeAt(1).toObject()["index_test"].toInt();
        }

        return ret;
    }

    void setIndex(bool vaxORtest,int i) {
        try {
            int old_indice_vax = getIndex(true);
            int old_indice_test= getIndex(false);
            QJsonArray jsonarray;
            QJsonObject q1,q2;
            if(vaxORtest){
                q1.insert("index_vax",i);
                jsonarray.append(q1);
                q2.insert("index_test",old_indice_test);
                jsonarray.append(q2);
            }else{
                q1.insert("index_vax",old_indice_vax);
                jsonarray.append(q1);
                q2.insert("index_test",i);
                jsonarray.append(q2);
            }


            QJsonObject o;
            o.insert(QString::fromStdString("config"), jsonarray);

            QFile file("resources/config.json");
            file.open(QFile::ReadWrite | QFile::Text);

            file.resize(0);

            file.write(QJsonDocument(o).toJson());
            file.close();

        } catch (QFile::FileError) {
            throw QFile::FileError();
        } catch (QJsonParseError::ParseError) {
            throw QJsonParseError::ParseError();
        }
    }
}
