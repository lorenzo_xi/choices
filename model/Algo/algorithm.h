#ifndef COVIDMANAGER_ALGORITHM_H
#define COVIDMANAGER_ALGORITHM_H

#include <iostream>
#include "model/Types/EnumTypes.h"
#include "model/Vector.h"
#include "model/DeepPtr.h"
#include "model/gerarchia/Base.h"
#include <QDateTime>
#include <model/gerarchia/Base.h>
#include <QFile>
#include <QJsonDocument>
#include <QString>
#include <QJsonArray>
#include <QJsonObject>
#include <model/gerarchia/Visita.h>
#include <model/gerarchia/Vaccinazione.h>
#include <model/gerarchia/Tampone.h>
#include <model/gerarchia/Sierologico.h>
#include <QTextDocument>
#include <QPdfWriter>
#include <QPainter>
#include <QFileDialog>

namespace Algorithm{


    /**
     *  @brief  conversione EnumTypes::Job in stringa corrispondente
     *  @param  en  enum da convertire
     *  @return stringa corrispondente all'enum
    */
    std::string fromEnumJobToStdString(const EnumTypes::Job &);

    /**
     *  @brief  conversione EnumTypes::Vax in stringa corrispondente
     *  @param  en  enum da convertire
     *  @return stringa corrispondente all'enum
    */
    std::string fromEnumVaxToStdString(const EnumTypes::Vax &);

    /**
     *  @brief  conversione EnumTypes::Tampone in stringa corrispondente
     *  @param  en  enum da convertire
     *  @return stringa corrispondente all'enum
    */
    std::string fromEnumTampToStdString(const EnumTypes::Tampone &);

    /**
     *  @brief  conversione EnumTypes::Risk in stringa corrispondente
     *  @param  en  enum da convertire
     *  @return stringa corrispondente all'enum
    */
    std::string fromEnumRiskToStdString(const EnumTypes::Risk &);

    /**
     *  @brief  conversione int => EnumTypes::Risk
     *  @param  int da convertire
     *  @return EnumTypes::Risk corrispondente all intero passato come parametro
    */

    EnumTypes::Risk fromStdStringToEnumRisk(const std::string &);


    /**
     *  @brief  conversione int => EnumTypes::Job
     *  @param  int da convertire
     *  @return EnumTypes::Job corrispondente all intero passato come parametro
    */
    EnumTypes::Job fromIntToEnumJob(int);


    /**
     *  @brief  conversione int => EnumTypes::Vax
     *  @param  int da convertire
     *  @return EnumTypes::Vax corrispondente all intero passato come parametro
    */
    EnumTypes::Vax fromIntToEnumVax(int);


    /**
     *  @brief  conversione std::string => EnumTypes::Job
     *  @param  stringa da convertire
     *  @return EnumTypes::Vax corrispondente alla stringa passato come parametro
    */
    EnumTypes::Vax fromStringToEnum(std::string);



    /**
     *  @brief  restituisce prezzo dei vaccini
     *  @param  EnumTypes::Vax il cui prezzo dev'essere restituito
     *  @return double corrispondente all' EnumTypes::Vax passato per parametro
    */
    double priceOfVax(const EnumTypes::Vax &);

    //algoritmi i/o

    /**
    *  @brief  legge il file json e alloca un oggetto nel container Vector<DeepPtr<Base*>> per ogni oggetto json nel file
    *  @param1 v       vettore nel quale saranno allocati gli oggetti letti dal file json, passato per riferimento
    *  @param2 path    percorso del file da leggere
    *  @param3 tipo    tipologia di oggeti json da leggere, e, di conseguenza, allocare
    */
    void readFile_toVectorDeepPtr(Vector<DeepPtr<Base>>& v, const  std::string & path, const std::string & tipo);

    /**
    *  @brief  legge gli oggetti 'tipo' dal file json (dato da path) e alloca un oggetto nel container Vector<DeepPtr<Base*>> per ogni oggetto json nel file
    *  @param2 path    percorso del file da leggere
    *  @param3 tipo    tipologia di oggeti json da leggere, e, di conseguenza, allocare
    *  @return QJsonArray che contiene gli oggetti del tipo 'tipo' presenti nel file letto
    */
    QJsonArray readFile_toJsonArray(const std::string & path, const std::string & tipo);

    /**
     * @brief   legge il file json "path" e restituisce un vettore di QString contenente gli attributi (relativi ai test) dell'index-esimo oggetto presente nel file json
     * @param   v       stringa di attributi
     * @param   path    percorso del file json da leggere
     * @param   index   indice oggetto del quale vogliamo gli attributi
     */
    void Test_readFile(Vector<QString>& v, const std::string & path, unsigned int index = 0);

    /**
     * @brief   legge il file json "path" e restituisce un vettore di QString contenente gli attributi (relativi alla vaccinazione) dell'index-esimo oggetto presente nel file json
     * @param   v       stringa di attributi
     * @param   path    percorso del file da leggere
     * @param   index   indice oggetto del quale vogliamo gli attributi
     */
    void Vax_readFile(Vector<QString>& v, const std::string &, unsigned int =0);

    /**
     * @brief aggiunge in coda al file json l'oggetto vobj
     * @param path  percorso del file json da leggere
     * @param tipo  oggetto che contiene una lista di oggetti nel file json
     * @param vobj  oggetto da scrivere
     */
    void write(const std::string & path , const std::string & tipo, const QJsonArray& vobj);

    /**
     * @brief aggiorna il file json aggiungendo all'oggetto "arrayname" presente nel file gli oggetto contenuti in v
     * @param v             vector di oggetti da scrivere nel file json
     * @param path          percorso del file da riscrivere
     * @param arrrayname    nome dell'oggetto principale (che contiene altri oggetti) da riscrivere nel file json
     */
    void update(Vector<DeepPtr<Base>>& v , const std::string & path, const std::string & arrrayname);

    /**
     * @brief crea una QString contentente un testo in formato HTML contenente i parametri passati (utilizzata per creare PDF ricevuta del test)
     * @param tipo          tipo di test per rilevare covid-19
     * @param cf            codice fiscale dell'utente che ha effettuato il test
     * @param email         email dell'utente che ha effettuato il test
     * @param recapito      recapito telefonico dell'utente che ha effettuato il test
     * @return              restituisce la QString creata che contiene un testo in formato HTML
     */
    QString get_testoPdf_test(int tipo, QString cf, QString email, QString recapito);

    /**
     * @brief crea una QString contentente un testo in formato HTML contenente i parametri passati (utilizzata per creare PDF ricevuta del vaccino)
     * @param nome          nome utente che ha effettuato il vaccino anti covid-19
     * @param cognome       codice fiscale dell'utente che ha effettuato il vaccino anti covid-19
     * @param dataNascita   email dell'utente che ha effettuato il vaccino anti covid-19
     * @param cf            codice fiscale dell'utente che ha effettuato il vaccino anti covid-19
     * @param email         email dell'utente che ha effettuato il vaccino anti covid-19
     * @param recapito      recapito telefonico dell'utente che ha effettuato il vaccino anti covid-19
     * @return              restituisce la QString creata che contiene un testo in formato HTML
     */
    QString get_testoPdf_vax(QString nome, QString cognome, QString dataNascita, QString cf, QString email, QString recapito, QString vax);


    /**
     * @brief   legge il file utilizzato per salvare gli indice delle visiti (e mantenere consistenza alla chiusura e alla riapertura del programama)
     * @param   b   flag che indica alla funzione se leggerre l'indice delle visiste tamponi (b==true) o dei test(b==false
     * @return      indice intero che indica la posizione della visita che dev'essere effettuata
     */
    int getIndex(bool b);


    /**
    * @brief   scrive sul file utilizzato per salvare gli indice delle visiti (e mantenere consistenza alla chiusura e alla riapertura del programama)
    * @param   b   flag che indica alla funzione se scrivere l'indice delle visiste tamponi (b==true) o dei test(b==false
    * @param   i   intero che viene scritto su, file
    */
    void setIndex(bool b,int i);

}
#endif //COVIDMANAGER_ALGORITHM_H
