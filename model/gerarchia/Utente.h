#ifndef COVIDMANAGER_UTENTE_H
#define COVIDMANAGER_UTENTE_H
#include "Base.h"
#include "../Types/Data.h"
#include <string>
#include <QtGui>

class Utente : public Base{
private:
    std::string      sesso;          //sesso
    Data             data_nascita;   //data di nascita

protected:
    std::string      nome;          //nome
    std::string      cognome;       //cognome
    std::string      c_f;           //codice fiscale

public:

    //costruttori
    Utente(std::string="",Data=Data(),std::string="",std::string="",std::string="");
    Utente(const QJsonObject&);

    //get
    std::string getNome() const;
    std::string getCognome() const;
    std::string getSesso() const;
    std::string getCF() const;
    Data        getData_Nascita() const;

    //set
    void setNome(std::string);
    void setCognome(std::string);
    void setSesso(std::string);
    void setCF(std::string);
    void setDataNascita(Data);

    //metodi virtuali
    virtual Utente* clone() const           override;
    void toJsonObj(QJsonObject& jobj) const override;

};
#endif //COVIDMANAGER_UTENTE_H
