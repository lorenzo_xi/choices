#ifndef COVIDMANAGER_SIEROLOGICO_H
#define COVIDMANAGER_SIEROLOGICO_H
#include "Test.h"

class Sierologico : public Test {
private:
    double quantita;                    //quantita di carica virale rilevata
    static double cutoff;               //cutoff sopra il quale si è considerati infettati
    static double prezzo_sierologico;   //prezzo del test sierologico

public:
    //costruttori
    Sierologico(Test t = Test(), double = 0);
    Sierologico(const QJsonObject &);

    //get
    double get_quantita() const;

    //set
    void set_quantita(double);

    //metodi virtuali
    void toJsonObj(QJsonObject &jobj) const override;
    virtual Sierologico* clone() const      override;
    virtual double prezzoTotale() const     override;
    virtual unsigned int durata() const     override;
};

#endif