#include "Evento.h"

//costruttori

Evento::Evento(int id, int durata_tot): ID_Evento(id), durata_totale(durata_tot){}

Evento::Evento(const QJsonObject & obj) {
    try{
        if(obj.isEmpty() || obj.empty()){
            throw QJsonParseError::ParseError();
        }else{
            ID_Evento   = obj["_id"].toInt();
            durata_totale = obj["_timing"].toInt();
        }
    }catch(QJsonParseError::ParseError){
        throw QJsonParseError::ParseError();
    }
}

Evento *Evento::clone() const {
    return new Evento(*this);
}

//get
int Evento::getID_Evento() const{
    return ID_Evento;
};

//set
void Evento::setID(int i){
    ID_Evento=i;
}

void Evento::setDurataTotale(unsigned int n){
    durata_totale=n;
}

unsigned int Evento::durata() const {
    return 0;
}

void Evento::toJsonObj(QJsonObject & jobj) const {
    jobj["_id"] = static_cast<int>(ID_Evento);
    jobj["_timing"] = static_cast<int>(durata_totale);
}
