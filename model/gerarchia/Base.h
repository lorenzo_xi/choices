#ifndef COVIDMANAGER_BASE_H
#define COVIDMANAGER_BASE_H

#include <QJsonObject>

class Base{
public:

    //metodi virtuali puri

    /**
    * @brief    clonazione polimorfa
    */
    virtual Base* clone() const = 0;

    /**
    * @brief    distruzione polimorfa con comportamento di default
    */
    virtual ~Base() = default;

    /**
     * @brief   aggiunge al QJSonObject passato come parametro una tupla che contiene i campi dati dell'oggetto chiamante e i relativi valori
     * @param   QJsonObject al quale vengono aggiunte tuple ("chiave",valore)
     */
    virtual void toJsonObj(QJsonObject&) const = 0;
};
#endif //COVIDMANAGER_BASE_H