#ifndef COVIDMANAGER_TAMPONE_H
#define COVIDMANAGER_TAMPONE_H
#include "Test.h"
#include "../Types/EnumTypes.h"
#include <iostream>


class Tampone : public Test {
private:
    EnumTypes::Tampone tipoTampone;     //tipologia di tampone (rapido, molecolare)
    static double prezzo_tampone;       //prezzo tampone

public:
    //costruttori
    Tampone(Test t = Test(), EnumTypes::Tampone = static_cast<EnumTypes::Tampone>(0));
    Tampone(const QJsonObject &);

    //get
    EnumTypes::Tampone getTipoTampone() const;

    //set
    void set_tipoTampone(EnumTypes::Tampone);

    //metodi virtuali
    virtual Tampone* clone() const              override;
    virtual void toJsonObj(QJsonObject&) const  override;
    virtual unsigned int durata() const         override;
    virtual double prezzoTotale() const         override;

};

#endif 