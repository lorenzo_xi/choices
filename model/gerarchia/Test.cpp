#include "Test.h"

//costruttori

Test::Test(Visita v, std::string az, double a, int e, Ora tr)
    : Visita(v), azienda_produttrice(az), accuratezza(a), esito(e), tempo_risposta(tr){}


Test::Test(const QJsonObject & obj): Visita(obj){
    try{
        if(obj.isEmpty() || obj.empty()){
            throw QJsonParseError::ParseError();
        }else{
            azienda_produttrice = obj["azienda_produttrice"].toString().toStdString();
            accuratezza         = obj["accuratezza"].toDouble();
            esito               = obj["esito"].toInt();
            tempo_risposta      = obj["tempo_risposta"].toInt();
        }
    }catch(QJsonParseError::ParseError){
        throw QJsonParseError::ParseError();
    }
}


Test *Test::clone() const {
    return new Test(*this);
}


//get

std::string Test::get_azienda_produttrice() const{
    return azienda_produttrice;
};
double Test::get_accuratezza() const{
    return accuratezza;
};
int Test::get_esito() const{
    return esito;
};

std::string Test::get_esito_str() const {
   std::string ris;
    switch (get_esito()) {
        case 0:ris ="0";
            break;
        case 1:ris ="1";
            break;
        case -1: ris ="?";
            break;
        default:    ris="X";
        break;
    }
    return ris;
};

Ora Test::get_tempo_risposta() const{
    return tempo_risposta;
};
double Test::getCutOff() const {
    return 0;
}



//set

void Test::set_azienda_produttrice(std::string az){
    azienda_produttrice=az;
};
void Test::set_accuratezza(double a){
    accuratezza=a;
};
void Test::set_esito( int b){
    esito=b;
};
void Test::set_tempo_risposta(Ora o){
    tempo_risposta=o;
}

void Test::toJsonObj(QJsonObject & jobj) const{
    Visita::toJsonObj(jobj);
    jobj["azienda_produttrice"]=QString::fromStdString(azienda_produttrice);
    jobj["accuratezza"]=accuratezza;
    jobj["esito"]=esito;
    jobj["tempo_risposta"]= QString::fromStdString(tempo_risposta.stringify());
}

unsigned int Test::durata() const{
    return Visita::durata()+120;
}

double Test::prezzoTotale() const {
    return Visita::prezzoTotale() + 5;
}
