#include "Sierologico.h"

//costruttori
Sierologico::Sierologico(Test t, double q): Test(t), quantita(q){}

Sierologico::Sierologico(const QJsonObject & obj) : Test(obj){

    try{
        if(obj.isEmpty() || obj.empty()){
            throw QJsonParseError::ParseError();
        }else{
            quantita = obj["quantita"].toDouble();
        }
    }catch(QJsonParseError::ParseError){
        throw QJsonParseError::ParseError();
    }
}
//get

double Sierologico::get_quantita() const{
    return quantita;
};

Sierologico *Sierologico::clone() const {
    return new Sierologico(*this);
}

//set

void Sierologico::set_quantita(double q){
    quantita=q;
};

void Sierologico::toJsonObj(QJsonObject &jobj) const {
    Test::toJsonObj(jobj);
    jobj["quantita"]    = quantita;
}
double Sierologico::prezzoTotale() const {
    return prezzo_sierologico + Visita::get_prezzo();
}

unsigned int Sierologico::durata() const{
    return Test::durata() + (60*15);
}

double Sierologico::cutoff = 250;
double Sierologico::prezzo_sierologico=15;
