#ifndef COVIDMANAGER_VISITA_H
#define COVIDMANAGER_VISITA_H
#include "Evento.h"
#include "model/Types/EnumTypes.h"
#include "../Types/Data.h"
#include "Paziente.h"
#include "OperatoreSanitario.h"
#include <iostream>

class Visita : public Evento {
private:
    Data data_visita;   //indica la data della visita

    bool prenotazione;  //indica se la visita è stata prenotata (true prenotata, false non prenotata)
    bool convenzione;   //indica se la visita è convenzionata (true convenzionata, false non convenzionata)
    bool stato;         //stato di erogazione della visita (true erogata, false non erogata)

    Paziente paziente;
    OperatoreSanitario riferimento;

    static double prezzo_visita;

public:
    //costruttori
    Visita(Evento= Evento(), Data=Data(),bool =false, bool = false, bool=false, Paziente=Paziente(),OperatoreSanitario=OperatoreSanitario());
    Visita(const QJsonObject&);

    //get
    bool isPrenotato() const;
    bool isConvenzionato() const;
    bool isDone() const;
    double get_prezzo() const;
    Data get_data_visita() const;
    Paziente* getPaziente();
    OperatoreSanitario getRiferimento() const;

    //set
    void set_prenotazione(bool);
    void set_convenzione(bool);
    void set_prezzo(double);
    void set_data_visita(Data);
    void set_stato(bool);
    void set_riferimento(OperatoreSanitario);

    //metodi virtuali
    virtual Visita* clone() const               override;
    virtual unsigned int durata() const         override;
    virtual void toJsonObj(QJsonObject&) const  override;

    /**
     * @brief   calcola prezzo totale della visita
     * @return  restituisce double che rappresenta il prezzo totale della visita
     */
    virtual double prezzoTotale() const;
};

#endif
