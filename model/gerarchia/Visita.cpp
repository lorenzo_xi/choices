#include "Visita.h"

double Visita::prezzo_visita = 50.0;

//costruttori

Visita::Visita(Evento e, Data dv, bool pr, bool c, bool stato, Paziente p, OperatoreSanitario os)
    : Evento(e), data_visita(dv), prenotazione(pr), convenzione(c), stato(stato), paziente(p), riferimento(os){}

Visita::Visita(const QJsonObject & obj) : Evento(obj){
    try{
        if(obj.isEmpty() || obj.empty()){
            throw QJsonParseError::ParseError();
        }else{

            data_visita = obj["data_visita"].toString().toStdString();
            prenotazione= obj["prenotazione"].toBool();
            convenzione = obj["convenzione"].toBool();
            stato       = obj["stato"].toBool();

            Paziente tmp_p = obj["paziente"].toObject();            
            paziente    = tmp_p;

            OperatoreSanitario tmp_op = obj["riferimento"].toObject();
            riferimento = tmp_op;
        }
    }catch(QJsonParseError::ParseError){
        throw QJsonParseError::ParseError();
    }
}

Visita *Visita::clone() const {
    return new Visita(*this);
}

//get
bool Visita::isPrenotato() const{
    return prenotazione;
}
bool Visita::isConvenzionato() const{
    return convenzione;
}
double Visita::get_prezzo() const{
    return convenzione ? (prezzo_visita) : (prezzo_visita / 2);
}
Data Visita::get_data_visita() const{
    return data_visita;
}

Paziente* Visita::getPaziente() {
    return &paziente;
}

bool Visita::isDone() const {
    return stato;
}

//set
void Visita::set_prenotazione(bool p){
    prenotazione=p;
}
void Visita::set_convenzione(bool c){
    convenzione=c;
}
void Visita::set_prezzo(double pr){
    prezzo_visita=pr;
}
void Visita::set_data_visita(Data dv){
    data_visita=dv;
}

void Visita::set_stato(bool s){
    stato=s;
}

void Visita::toJsonObj(QJsonObject & jobj) const{
    Evento::toJsonObj(jobj);
    QJsonObject tmp_paziente, tmp_operatore;
    paziente.Paziente::toJsonObj(tmp_paziente);
    riferimento.OperatoreSanitario::toJsonObj(tmp_operatore);

    jobj["data_visita"] =   QString::fromStdString(data_visita.stringify());
    jobj["prenotazione"]=   prenotazione;
    jobj["convenzione"] =   convenzione;
    jobj["stato"]       =   stato;
    jobj["prezzo_visita"]=  prezzo_visita;
    jobj["paziente"]    =   tmp_paziente;
    jobj["riferimento"]=    tmp_operatore;
}

double Visita::prezzoTotale() const{
    return prezzo_visita;
}

void Visita::set_riferimento(OperatoreSanitario op) {
    riferimento = op;
}

OperatoreSanitario Visita::getRiferimento() const {
    return riferimento;
}

unsigned int Visita::durata() const {
    return 300;
}
