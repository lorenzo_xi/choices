#include "Vaccinazione.h"

Vaccinazione::Vaccinazione(Visita v, EnumTypes::Vax az, std::string l, double e, bool r)
    : Visita(v), lotto(l), efficacia(e), richiamo(r){
    azienda_produttrice=az;
}


Vaccinazione::Vaccinazione(const QJsonObject & obj): Visita(obj) {
    try{
        if(obj.isEmpty() || obj.empty()){
            throw QJsonParseError::ParseError();
        }else{
            azienda_produttrice = Algorithm::fromStringToEnum( obj["azienda_produttrice"].toString().toStdString());
            lotto               = obj["lotto"].toString().toStdString();
            efficacia           = obj["efficacia"].toDouble();
            richiamo            = obj["richiamo"].toBool();
        }
    }catch(QJsonParseError::ParseError){
        throw QJsonParseError::ParseError();
    }
}

//get
EnumTypes::Vax Vaccinazione::get_azienda_produttrice() const{
    return azienda_produttrice;
}
std::string Vaccinazione::get_lotto() const{
    return lotto;
}
double Vaccinazione::get_efficacia() const{
    return efficacia;
}
bool Vaccinazione::isRichiamo() const{
    return richiamo;
}

void Vaccinazione::toJsonObj(QJsonObject & jobj) const{
    Visita::toJsonObj(jobj);
    jobj["azienda_produttrice"]=QString::fromStdString(Algorithm::fromEnumVaxToStdString(azienda_produttrice));
    jobj["lotto"]=QString::fromStdString(lotto);
    jobj["efficacia"]=efficacia;
    jobj["richiamo"]=richiamo;
}

Vaccinazione *Vaccinazione::clone() const {
    return new Vaccinazione(*this);
}

double Vaccinazione::prezzoTotale() const{
    return Visita::prezzoTotale() + Algorithm::priceOfVax(azienda_produttrice);
}

unsigned int Vaccinazione::durata() const{
    return Visita::durata() + (60*5);
}
