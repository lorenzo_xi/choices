#include "Tampone.h"

//costruttori

Tampone::Tampone(Test t, EnumTypes::Tampone tp): Test(t), tipoTampone(tp){}

Tampone::Tampone(const QJsonObject & obj) : Test(obj){

    try{
        if(obj.isEmpty() || obj.empty()){
            throw QJsonParseError::ParseError();
        }else{
            tipoTampone = static_cast<EnumTypes::Tampone>(obj["tipo_tampone"].toInt());
        }
    }catch(QJsonParseError::ParseError){
        throw QJsonParseError::ParseError();
    }
}


Tampone *Tampone::clone() const {
    return new Tampone(*this);
}


// get

EnumTypes::Tampone Tampone::getTipoTampone() const{
    return tipoTampone;
};

// set

void Tampone::set_tipoTampone(EnumTypes::Tampone tp){
    tipoTampone=tp;
}

void Tampone::toJsonObj(QJsonObject & jobj) const {
    Test::toJsonObj(jobj);
    jobj["tipo_tampone"] = static_cast<int>(tipoTampone);
};
double Tampone::prezzoTotale() const {
    return prezzo_tampone + Visita::get_prezzo() + (tipoTampone == EnumTypes::Tampone::Antigenico ? 10 : 50);
}

unsigned int Tampone::durata() const{
    return Test::durata() + (tipoTampone == EnumTypes::Tampone::Antigenico ? (60*5) : (60*10) );
}

double Tampone::prezzo_tampone = 10.0;
