#include "Utente.h"

//costruttori
Utente::Utente(std::string n, Data dob, std::string c,std::string s, std::string cf)
    : sesso(s), data_nascita(dob),nome(n),cognome(c), c_f(cf)
    { }

Utente::Utente(const QJsonObject& obj) {
    try{
            nome = obj["nome"].toString().toStdString();
            cognome = obj["cognome"].toString().toStdString();

            sesso = obj["sesso"].toString().toStdString();
            c_f =  obj["cf"].toString().toStdString();

            Data d = obj["data_nascita"].toString().toStdString();
            data_nascita = d;

    }catch(...){
        throw QJsonParseError::ParseError();
    }
}

Utente *Utente::clone() const {
    return new Utente(*this);
}


//get
std::string Utente::getNome() const{
    return nome;
}

std::string Utente::getCognome() const{
    return cognome;
}

std::string Utente::getSesso() const{
    return sesso;
}

std::string Utente::getCF() const{
    return c_f;
}

Data Utente::getData_Nascita() const{
    return data_nascita;
}

//set
void Utente::setNome(std::string n){
    nome=n;
}

void Utente::setCognome(std::string c){
    cognome=c;
}

void Utente::setSesso(std::string s){
    sesso=s;
}

void Utente::setCF(std::string cf){
    c_f=cf;
}

void Utente::setDataNascita(Data dob){
    data_nascita=dob;
}

void Utente::toJsonObj(QJsonObject &jobj) const {
    jobj["nome"]            =   QString::fromStdString(nome);
    jobj["cognome"]         =   QString::fromStdString(cognome);
    jobj["sesso"]           =   QString::fromStdString(sesso);
    jobj["cf"]              =   QString::fromStdString(c_f);
    jobj["data_nascita"]    =   QString::fromStdString(data_nascita.stringify());
}
