#ifndef COVIDMANAGER_TEST_H
#define COVIDMANAGER_TEST_H
#include "Visita.h"
#include <iostream>
#include <model/Types/Ora.h>

class Test : public Visita {
private:
    std::string azienda_produttrice;    //azienda che produce il test
    double accuratezza;                 //accuratezza del test
    int esito;                          //1 positivo, 0 negativo, -1 in elaborazione
    Ora tempo_risposta;                 //tempo per l'elaborazione del test in laboratorio

public:
    //costruttori
    Test(Visita v = Visita(), std::string = "", double =0, int =-1, Ora=Ora());
    Test(const QJsonObject&);


    //get
    std::string get_azienda_produttrice() const;
    double get_accuratezza() const;
    int get_esito() const;
    std::string get_esito_str() const;
    Ora get_tempo_risposta() const;
    double getCutOff() const;

    //set
    void set_azienda_produttrice(std::string);
    void set_accuratezza(double);
    void set_esito(int);
    void set_tempo_risposta(Ora);

    //metodi virtuali
    virtual Test* clone() const override;
    virtual void toJsonObj(QJsonObject&) const  override;
    virtual unsigned int durata() const         override;
    virtual double prezzoTotale() const         override;
};

#endif