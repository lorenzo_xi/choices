#ifndef COVIDMANAGER_OPERATORESANITARIO_H
#define COVIDMANAGER_OPERATORESANITARIO_H
#include "Utente.h"
#include "model/Types/EnumTypes.h"

class OperatoreSanitario : public Utente{

private:
    int matricola;              //matricola identificativa dell'Operatore Sanitario
    std::string ospedale;       //ospedale o struttura dove lavora l'Operatore Sanitario
    EnumTypes::Job professione; //mansione dell'Operatore Sanitario
public:
    //costruttori
    OperatoreSanitario(Utente=Utente(), int=0, std::string="ospedale_non_specificato",EnumTypes::Job=static_cast<EnumTypes::Job>(0));
    OperatoreSanitario(const QJsonObject&);

    //get
    int getMatricola() const;
    std::string getOspedale() const;
    EnumTypes::Job getProfessione() const;

    //set
    void setMatricola(int);
    void setOspedale(std::string);
    void setProfessione(EnumTypes::Job);

    //metodi
    /**
     * @brief   crea una stringa che rappresenta l'oggetto "Operatore Sanitario" con i valori dei campi dati dell'oggetto chiamante
     * @return  stringa che rappresenta l'oggetto chiamante
     */
    std::string stringify() const ;

    //metodi virtuali
    virtual OperatoreSanitario* clone() const   override;

    void toJsonObj(QJsonObject& jobj) const     override;
};


#endif //COVIDMANAGER_OPERATORESANITARIO_H