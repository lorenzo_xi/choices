#ifndef COVIDMANAGER_VACCINAZIONE_H
#define COVIDMANAGER_VACCINAZIONE_H
#include "Visita.h"
#include <iostream>
#include "model/Algo/algorithm.h"

class Vaccinazione : public Visita {
private:
    EnumTypes::Vax azienda_produttrice;     //azienda produttrice del vaccino
    std::string lotto;                      //lotto di provenienza del vaccino
    double efficacia;                       //efficacia del vaccino espressa in %
    bool   richiamo;                        //inddica se vaccinazione è richiamo (==true) oppure no (==false)
public:

    //costruttori
    Vaccinazione(Visita=Visita(), EnumTypes::Vax = EnumTypes::AstraZeneca, std::string = "", double = 0, bool = false);
    Vaccinazione(const QJsonObject&);

    //get
    EnumTypes::Vax get_azienda_produttrice() const;
    std::string get_lotto() const;
    double get_efficacia() const;
    bool isRichiamo() const;

    //metodi virtuali
    virtual Vaccinazione* clone() const         override;
    virtual void toJsonObj(QJsonObject&) const  override;
    virtual double prezzoTotale() const         override;
    virtual unsigned int durata() const         override;
};

#endif