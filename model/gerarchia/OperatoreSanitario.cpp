#include "OperatoreSanitario.h"
#include "model/Algo/algorithm.h"

//costruttori
OperatoreSanitario::OperatoreSanitario(Utente u, int m, std::string o, EnumTypes::Job p)
    : Utente(u), matricola(m), ospedale(o), professione(p)
    { }

OperatoreSanitario::OperatoreSanitario(const QJsonObject& obj): Utente(obj) {


    try{
            matricola   = obj["matricola"].toInt();
            ospedale    = obj["ospedale"].toString().toStdString();
            if(obj["professione"].toBool()){
                professione = EnumTypes::Doctor;
            }else{
                professione = EnumTypes::Nurse;
            }
    }catch(...){
        throw QJsonParseError::ParseError();
    }
}

OperatoreSanitario *OperatoreSanitario::clone() const {
    return new OperatoreSanitario(*this);
}


//get
int OperatoreSanitario::getMatricola() const{
    return matricola;
}

std::string OperatoreSanitario::getOspedale() const{
    return ospedale;
}

EnumTypes::Job OperatoreSanitario::getProfessione() const{
    return professione;
}

//set
void OperatoreSanitario::setMatricola(int m){
    matricola=m;
}

void OperatoreSanitario::setOspedale(std::string o){
    ospedale=o;
}

void OperatoreSanitario::setProfessione(EnumTypes::Job p){
    professione=p;
}

void OperatoreSanitario::toJsonObj(QJsonObject &jobj) const {
    Utente::toJsonObj(jobj);
    jobj["matricola"]       =matricola;
    jobj["ospedale"]        =QString::fromStdString(ospedale);
    jobj["professione"]     =professione;
}

std::string OperatoreSanitario::stringify() const {
    std::string p = Algorithm::fromEnumJobToStdString(professione);
    return (nome + " " + cognome + "\n" + c_f + "\nmansione: " + p);
}
