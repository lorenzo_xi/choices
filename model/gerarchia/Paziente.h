#ifndef COVIDMANAGER_PAZIENTE_H
#define COVIDMANAGER_PAZIENTE_H
#include "Utente.h"
#include <model/Types/Data.h>
#include <model/Types/EnumTypes.h>
#include <model/Types/HealthData.h>
#include <model/Types/EnumTypes.h>
#include <QJsonParseError>

class Paziente : public Utente {
private:

    std::string email;              //email dell'Utente
    std::string telefono;           //recapito telefonico dell'Utente
    HealthData cartella_clinica;    //cartella clinica dell'utente (dati sanitari)

public:

    //costruttori
    Paziente(Utente=Utente(), std::string e = "", std::string t="", HealthData hd = HealthData());
    Paziente(const QJsonObject&);

    //get
    std::string getEmail() const;
    std::string getTelefono() const;
    HealthData getCartellaClinica() const;
    EnumTypes::Risk getRischioPaziente();

    //set
    void setEmail(std::string);
    void setTelefono(std::string);
    void setCartellaClinica(HealthData);


    //metodi
    /**
     * @brief   calcola il rischio di morte Covid-19 del paziente in base alla sua cartella clinica
     * @return  EnumTypes::Risk che rappresenta il rischio del paziente
     */
    EnumTypes::Risk calcoloRischioPaziente();

    //metodi virtuali
    virtual Paziente* clone() const         override;

    void toJsonObj(QJsonObject& jobj) const override;
};


#endif //COVIDMANAGER_PAZIENTE_H