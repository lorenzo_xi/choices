#ifndef COVIDMANAGER_EVENTO_H
#define COVIDMANAGER_EVENTO_H

#include <QJsonObject>
#include <model/Types/Ora.h>
#include <model/Types/DataOra.h>
#include "Base.h"
#include <QtCore>

class Evento: public Base{
private:
    unsigned int ID_Evento;         //numero identificativo dell'evento
    unsigned int durata_totale;     //durata temporale dell'evento (in h)

public:

    //costruttori
    Evento(int = 0, int = 0);
    Evento(const QJsonObject&);


    //get
    int getID_Evento() const;

    //set
    void setID(int);
    void setDurataTotale(unsigned int);

    //metodi virtuali
    virtual void toJsonObj(QJsonObject&) const  override;

    virtual Evento* clone() const               override;

    /**
     * @brief   restituisce la durata temporale dell'oggetto passato (in ore)
     * @return  un intero (senza segno) che rappresenta la durata temporale in ore dell'oggetto chiamante
    */
    virtual unsigned int durata()const; //durata in ore
};
#endif