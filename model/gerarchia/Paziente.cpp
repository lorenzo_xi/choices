#include "Paziente.h"

//costruttori
Paziente::Paziente(Utente u, std::string e, std::string t, HealthData hd)
    : Utente(u), email(e), telefono(t), cartella_clinica(hd)
    { }

Paziente::Paziente(const QJsonObject & obj): Utente(obj){
    try{
        email               = obj["email"].toString().toStdString();
        telefono            = obj["telefono"].toString().toStdString();
        cartella_clinica    = obj["cartella_clinica"].toString().toStdString();

    }catch(...){
        throw QJsonParseError::ParseError();
    }
}


Paziente *Paziente::clone() const {
    return new Paziente(*this);
}


//get
std::string Paziente::getEmail() const{
    return email;
}

std::string Paziente::getTelefono() const{
    return telefono;
}

HealthData Paziente::getCartellaClinica() const{
    return cartella_clinica;
}

//set
void Paziente::setEmail(std::string e){
    email = e;
}

void Paziente::setTelefono(std::string t){
    telefono = t;
}

void Paziente::setCartellaClinica(HealthData cc){
    cartella_clinica = cc;
}

EnumTypes::Risk Paziente::getRischioPaziente(){
    return  calcoloRischioPaziente();
}

EnumTypes::Risk Paziente::calcoloRischioPaziente() {

    int num =Paziente::cartella_clinica.getPatologie();
    int rischio = 0;
    EnumTypes::Risk tmp =EnumTypes::Basso;

    if(num == 1){
        rischio+=5;
    }else if(num == 2){
        rischio+=30;
    }else if (num > 2){
        rischio+=50;
    }

    rischio+=Paziente::getData_Nascita().getAnno()-2021;

    if(rischio < 20){
        tmp=EnumTypes::Basso;
    }else if(rischio>=20 && rischio <= 50){
        tmp=EnumTypes::Medio;
    }else{
        tmp=EnumTypes::Alto;
    }

    return tmp;
}

void Paziente::toJsonObj(QJsonObject &jobj) const {
    Utente::toJsonObj(jobj);
    jobj["email"]=QString::fromStdString(email);
    jobj["telefono"]=QString::fromStdString(telefono);
    jobj["cartella_clinica"] = QString::fromStdString(cartella_clinica.stringify());
}
