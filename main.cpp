#include <QApplication>
#include <QFileDialog>
#include "controller/Controller.h"
#include <QDesktopServices>
#include <QtGui>
#include <model/gerarchia/Tampone.h>
#include <model/gerarchia/Sierologico.h>

int main(int argc, char *argv[]){

    QApplication a(argc, argv);
    Controller x(nullptr);
    a.setWindowIcon((QIcon("://resources/img/CM.png")));

    QPalette pal = a.palette();
    QColor color(255, 252, 239 );
    pal.setColor(QPalette::Window, color);
    a.setPalette(pal);
    

    return a.exec();
}
