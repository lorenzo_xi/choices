---

title:  Progetto di Programmazione ad Oggetti (CdL Informatica UniPD) a.a. 2020/2021.

author: Silvia Giro, Perinello Lorenzo

date:   Giugno 2020

comment: note utili per compilazione e avvio

---

# CovidManager

## Build e run

1. qmake CovidManager.pro
2. make
3. ./CovidManager

## Manuale utente
il manuale utente è disbonibile in [questo file](UserGuide.md)
